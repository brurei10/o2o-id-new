
# O2O Id

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Sistema responsável integração e autorização de clientes internos e externos para uso das O2O Apis.

  - Liberação de serviços para gestão e controle de Autorização.
  - Consumidor de protocolos e credenciais de acesso via Oauth 2.

# New Features!

- Autorização de Sistemas Clientes;
- Cadastrar Usuario;
- Pesquisa de Usuários;
- Alteração de Senha de Usuário;
- Reset de Senha;
- Remoção Lógica;
- Atualização do Usuário;
- Convite de Usuário;
- Pesquisa de Usuário

COMO PLUS:
 - Disponibilização do Swagger para auxiliar no uso e entendimento das regras do negócio;

### Tech

#### Arquitetura da Integração:


Contrato:

{
    "email": "bruno.santos@yahoo.com.br",
    "name": "BRUNO",
    "password": "123456",
    "active": 1,
    "login": "bruno.santos@yahoo.com.br",
    "is_admin": 1,
    "cpfCnpj": "11111111111",
    "phone": "91991919191",
    "id_external": "VarigUalata",
    "wats_phone": "91991413557",
    "customer": {
        "id": 1
    },
    "profile": {
        "id": 1
    },
    "product": [
        {
            "id": 1
        }
    ]
}


## :rocket: Technologies

This project was developed with the following technologies:

- [Node.js][nodejs]
- [TypeScript][typescript]
- [Express.js][express]
- [TypeORM][typeorm]
- [MySql][mysql]



Estrutura da aplicação
Estamos seguindo a estrategia de isolamento do dominio, para isso estamos seguindo conceitos importantes de desenvolvimento como clean arquitecture, DDD e Hexagonal.

A estrutura dos pacotes ficou definida como:

    │   │                       ├───application
    │   │                       │   ├───config
    │   │                       │   ├───controller
    │   │                       │
    │   │                       ├───domain
    │   │                       │   ├───port
    │   │                       │   │   ├───inbound
    │   │                       │   │   └───outbound
    │   │                       │   └───service
    │   │                       └───infrastructure
    │   │                           └───amqp

Sendo:

Application: Porta de entrada da nossa aplicação, contem tudo que precisamos para conectar outros sistemas com o nosso domínio como, por exemplo, o controller com os endpoints da aplicação, seguindo o contrato da api. A comunicação dessa camada com o domain é feita através de interfaces definidas no inbound no pacote de domínio.

Domain: Essa camada vai conter toda a nossa regra de negócios, é aqui que fazemos a implementação dos nossos services (casos de uso) e das nossas classes de domínio.

Infrastructure: Toda a comunicação e configuração externa do nosso sistema. A comunicação com esse modulo é feito através de interfaces definidas no outbound do nosso sistema, visando o desacoplamento da solução com os detalhes de infraestrutura.


## :information_source: How To Use

To clone and run this application, you'll need [Git](https://git-scm.com), [Node.js][nodejs], [MySql][mysql] installed on your computer.

From your command line:

### Install API

```bash
# Clone this repository
$ git clone https://git.o2osaude.com.br/developers/o2o-id.git

# Go into the repository
$ cd o2o-id

# Install dependencies
$ npm install

# Run Migrates
$ npm update-database

# Start server
$ npm run dev

running on port 5050
```

Vamos preparar um registro de cliente dinâmico com o OAuth2.0. O OAuth2.0 é uma estrutura de autorização que permite obter acesso limitado a contas de usuário em um serviço HTTP. O cliente OAuth2.0 é o aplicativo que deseja acessar a conta do usuário. Este cliente pode ser um aplicativo da web externo, um agente de usuário ou apenas um cliente nativo.

Para obter o registro de cliente dinâmico, vamos armazenar as credenciais no banco de dados, em vez da configuração codificada. O aplicativo que iremos estender foi inicialmente descrito no tutorial Spring REST API + OAuth2 .

### O Esquema DB
A tabela principal para gestão e controle de clientes (APIS) é a seguinte:

create table oauth_client_details (
    client_id VARCHAR(256) PRIMARY KEY,
    resource_ids VARCHAR(256),
    client_secret VARCHAR(256),
    scope VARCHAR(256),
    authorized_grant_types VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities VARCHAR(256),
    access_token_validity INTEGER,
    refresh_token_validity INTEGER,
    additional_information VARCHAR(4096),
    autoapprove VARCHAR(256)
);
Os campos mais importantes do oauth_client_details nos quais devemos nos concentrar são:

client_id - para armazenar o id de clientes recém-registrados
client_secret - para armazenar a senha dos clientes
access_token_validity - que indica se o cliente ainda é válido
autoridades - para indicar quais funções são permitidas com determinado cliente
escopo - ações permitidas, por exemplo, escrever status no Facebook etc.
autorizado_grant_types , que fornece informações sobre como os usuários podem fazer login em um cliente específico (em nosso caso de exemplo, é um formulário de login com senha)
Observe que cada cliente tem um relacionamento de um para muitos com os usuários, o que naturalmente significa que vários usuários podem utilizar um único cliente .

### Vamos Persistir Alguns Clientes
Com a definição de esquema SQL, podemos finalmente criar alguns dados no sistema - e basicamente definir um cliente.

Vamos usar o script data.sql - que Spring Boot irá executar por padrão - para inicializar o banco de dados. O mesmo executa as seguintes persistencias abaixo:

INSERT INTO oauth_client_details
	(client_id, client_secret, scope, authorized_grant_types,
	web_server_redirect_uri, authorities, access_token_validity,
	refresh_token_validity, additional_information, autoapprove)
VALUES
	("fooClientIdPassword", "secret", "foo,read,write,
	"password,authorization_code,refresh_token", null, null, 36000, 36000, null, true);
A descrição dos campos mais importantes em oauth_client_details é fornecida na seção anterior.

E aqui está a lógica para obter o token de acesso:

private String obtainAccessToken(String clientId, String username, String password) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientId);
    params.put("username", username);
    params.put("password", password);
    Response response = RestAssured.given().auth().preemptive()
      .basic(clientId, "secret").and().with().params(params).when()
      .post("http://localhost:8090/spring-security-oauth-server/oauth/token");
    return response.jsonPath().getString("access_token");
}

## :battery: How does it work?

## :construction: under construction :construction:

Thanks! :metal:

[nodejs]: https://nodejs.org/
[typescript]: https://www.typescriptlang.org/
[express]: https://expressjs.com/
[typeorm]: https://typeorm.io/
[mysql]: https://dev.mysql.com/downloads/mysql/
[insomnia]: https://insomnia.rest/
[vs]: https://code.visualstudio.com/
[vceditconfig]: https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig
[vceslint]: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
[prettier]: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

import { ProfileModel } from '@/domain/models'
import { faker } from '@faker-js/faker'

export const mockProfileModel = (): ProfileModel => ({
  id: faker.datatype.number(),
  accountId: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  name: faker.random.words()
})

export const mockProfileParams = (): ProfileModel => ({
  accountId: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  name: faker.random.words()
})

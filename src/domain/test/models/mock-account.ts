import { AccountModel } from '@/domain/models'
import { faker } from '@faker-js/faker'
import { cnpj } from 'cpf-cnpj-validator'

export const mockAccountModel = (): AccountModel => ({
  id: faker.datatype.number(),
  userOwnerId: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  contractCode: faker.random.alphaNumeric(),
  cpfCnpjOWner: cnpj.generate(),
  empresaContrato: faker.random.alphaNumeric(),
  filialContrato: faker.random.alphaNumeric(),
  productName: faker.random.words(),
  isActive: true
})

export const mockAccountParams = (): AccountModel => ({
  userOwnerId: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  contractCode: faker.random.alphaNumeric(),
  cpfCnpjOWner: cnpj.generate(),
  empresaContrato: faker.random.alphaNumeric(),
  filialContrato: faker.random.alphaNumeric(),
  productName: faker.random.words(),
  isActive: true
})

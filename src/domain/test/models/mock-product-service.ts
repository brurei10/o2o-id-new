import { ProductServiceModel } from '@/domain/models'
import { faker } from '@faker-js/faker'

export const mockProductServiceParams = (): ProductServiceModel => {
  const id = faker.datatype.number()
  return {
    id,
    ownerProductId: id,
    description: faker.random.words(),
    name: faker.random.words()
  }
}

export const mockProductServiceModel = (): ProductServiceModel => ({
  id: faker.datatype.number(),
  ownerProductId: faker.datatype.number(),
  description: faker.random.words(),
  name: faker.random.words()
})

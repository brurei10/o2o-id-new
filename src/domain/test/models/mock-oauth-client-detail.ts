import { Oauth2ClientDetailsModel } from '@/domain/models'
import { faker } from '@faker-js/faker'

export const mockOauth2ClientDetailsModel = (): Oauth2ClientDetailsModel => ({
  id: faker.datatype.uuid(),
  accessTokenValidity: faker.datatype.number(),
  authorities: faker.datatype.string(),
  clientId: faker.datatype.uuid(),
  clientSecret: faker.internet.userName(),
  grantTypes: faker.internet.userAgent(),
  redirectUris: faker.internet.url(),
  refreshTokenValidity: faker.datatype.number(),
  resources: faker.datatype.string(),
  scopes: faker.datatype.string(),
  secretRequired: faker.datatype.boolean(),
  createdAt: faker.datatype.datetime(),
  updatedAt: faker.datatype.datetime()
})

export const mockOauth2ClientDetailsParams = (): Oauth2ClientDetailsModel => ({
  accessTokenValidity: faker.datatype.number(),
  authorities: faker.datatype.string(),
  clientId: faker.datatype.uuid(),
  clientSecret: faker.internet.userName(),
  grantTypes: faker.internet.userAgent(),
  redirectUris: faker.internet.url(),
  refreshTokenValidity: faker.datatype.number(),
  resources: faker.datatype.string(),
  scopes: faker.datatype.string(),
  secretRequired: faker.datatype.boolean(),
  createdAt: faker.datatype.datetime(),
  updatedAt: faker.datatype.datetime()
})

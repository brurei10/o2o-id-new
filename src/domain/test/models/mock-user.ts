import { UserModel } from '@/domain/models'
import { faker } from '@faker-js/faker'
import { cpf } from 'cpf-cnpj-validator'

export const mockUserModel = (): UserModel => ({
  id: faker.datatype.number(),
  cpfCnpj: cpf.generate(),
  email: faker.internet.email(),
  idExternal: faker.internet.userName(),
  login: faker.internet.userName(),
  name: faker.name.fullName(),
  password: faker.internet.password(),
  phone: '31995678907',
  userLanguage: faker.random.word(),
  whatsPhone: '31995678907',
  token: faker.datatype.uuid(),
  firstAccess: true,
  active: true,
  lastAccess: faker.datatype.datetime()
})

export const mockUserParams = (): UserModel => ({
  cpfCnpj: cpf.generate(),
  email: faker.internet.email(),
  idExternal: faker.internet.userName(),
  login: faker.internet.userName(),
  name: faker.name.fullName(),
  password: faker.internet.password(),
  phone: '31995678907',
  userLanguage: faker.random.word(),
  whatsPhone: '31995678907',
  token: faker.datatype.uuid(),
  firstAccess: true,
  active: true,
  lastAccess: faker.datatype.datetime()
})

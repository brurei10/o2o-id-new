import { AccessCodeModel } from '@/domain/models'
import { faker } from '@faker-js/faker'

export const mockAccessCodeParams = (): AccessCodeModel => ({
  userId: faker.datatype.number(),
  code: faker.datatype.uuid(),
  validAt: faker.datatype.datetime(),
  type: 'reset_password',
  createdAt: faker.datatype.datetime(),
  updatedAt: faker.datatype.datetime()
})

export const mockAccessCodeModel = (): AccessCodeModel => ({
  id: faker.datatype.number(),
  userId: faker.datatype.number(),
  type: 'reset_password',
  code: faker.datatype.uuid(),
  validAt: faker.datatype.datetime(),
  createdAt: faker.datatype.datetime(),
  updatedAt: faker.datatype.datetime()
})

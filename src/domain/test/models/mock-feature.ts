import { FeatureModel } from '@/domain/models'
import { faker } from '@faker-js/faker'

export const mockFeatureModel = (): FeatureModel => ({
  id: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  name: faker.random.words()
})

export const mockFeatureParams = (): FeatureModel => ({
  id: faker.datatype.number(),
  productServiceId: faker.datatype.number(),
  name: faker.random.words()
})

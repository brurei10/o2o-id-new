import { AddUser } from '@/domain/usecases'
import { faker } from '@faker-js/faker'
import { cpf } from 'cpf-cnpj-validator'

export const mockUserInput = (): AddUser.Input => ({
  cpfCnpj: cpf.generate(),
  email: faker.internet.email(),
  idExternal: faker.internet.userName(),
  login: faker.internet.userName(),
  name: faker.name.fullName(),
  password: faker.internet.password(),
  phone: '31995678907',
  userLanguage: faker.random.word(),
  whatsPhone: faker.phone.number(),
  product: {
    id: faker.datatype.number(),
    accounts: [{
      contractCode: faker.random.alphaNumeric(),
      empresaContrato: faker.random.alphaNumeric(),
      productName: faker.company.name(),
      productServiceId: faker.datatype.number(),
      profiles: [{
        features: [{
          id: faker.datatype.number()
        }]
      }]
    }]
  }
})

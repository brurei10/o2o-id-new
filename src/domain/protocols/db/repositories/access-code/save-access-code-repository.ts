import { AccessCodeModel } from '@/domain/models'

export interface SaveAccessCodeRepository {
  save: (input: SaveAccessCodeRepository.Input) => Promise<SaveAccessCodeRepository.Output>
}

export namespace SaveAccessCodeRepository {
  export type Input = {
    id?: number
    userId: number
    code: string
    type: 'reset_password'
    validAt: Date
  }
  export type Output = AccessCodeModel
}

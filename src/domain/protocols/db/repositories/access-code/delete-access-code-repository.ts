export interface DeleteAccessCodeRepository {
  delete: (input: DeleteAccessCodeRepository.Input) => Promise<DeleteAccessCodeRepository.Output>
}

export namespace DeleteAccessCodeRepository {
  export type Input = {
    id?: number
  }
  export type Output = number
}

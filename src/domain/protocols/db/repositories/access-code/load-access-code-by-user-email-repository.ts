import { AccessCodeModel } from '@/domain/models'

export interface LoadAccessCodeByUserEmailRepository {
  loadByUser: (input: LoadAccessCodeByUserEmailRepository.Input) => Promise<LoadAccessCodeByUserEmailRepository.Output>
}

export namespace LoadAccessCodeByUserEmailRepository {
  export type Input = {
    email: string
  }
  export type Output = AccessCodeModel
}

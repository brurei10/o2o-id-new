import { AccessCodeModel } from '@/domain/models'

export interface LoadAccessCodeByIdRepository {
  loadById: (input: LoadAccessCodeByIdRepository.Input) => Promise<LoadAccessCodeByIdRepository.Output>
}

export namespace LoadAccessCodeByIdRepository {
  export type Input = {
    id: number
  }
  export type Output = AccessCodeModel
}

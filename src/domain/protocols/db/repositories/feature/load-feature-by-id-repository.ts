import { FeatureModel } from '@/domain/models'

export interface LoadFeatureByIdRepository {
  loadById: (input: LoadFeatureByIdRepository.Input) => Promise<LoadFeatureByIdRepository.Output>
}

export namespace LoadFeatureByIdRepository {
  export type Input = {
    id: number
  }
  export type Output = FeatureModel
}

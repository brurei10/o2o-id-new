import { ProductServiceModel, UserModel } from '@/domain/models'

export interface AddUserRepository {
  add: (input: AddUserRepository.Input) => Promise<AddUserRepository.Output>
}

export namespace AddUserRepository {
  export type Input = {
    cpfCnpj: string
    email: string
    idExternal: string
    login: string
    name: string
    password: string
    phone: string
    userLanguage: string
    whatsPhone: string
    productServices?: ProductServiceModel[]
  }
  export type Output = UserModel
}

import { UserModel } from '@/domain/models'

export interface LoadUserByCredentialsRepository {
  loadByCredentials: (input: LoadUserByCredentialsRepository.Input) => Promise<LoadUserByCredentialsRepository.Output>
}

export namespace LoadUserByCredentialsRepository {
  export type Input = {
    login: string
  }
  export type Output = UserModel
}

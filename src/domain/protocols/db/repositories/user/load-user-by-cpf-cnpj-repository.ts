import { UserModel } from '@/domain/models'

export interface LoadUserByCpfCnpjRepository {
  loadByCpfCnpj: (input: LoadUserByCpfCnpjRepository.Input) => Promise<LoadUserByCpfCnpjRepository.Output>
}

export namespace LoadUserByCpfCnpjRepository {
  export type Input = {
    cpfCnpj: string
  }
  export type Output = UserModel
}

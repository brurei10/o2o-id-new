import { UserModel } from '@/domain/models'

export interface UpdatePasswordRepository {
  updatePassword: (input: UpdatePasswordRepository.Input) => Promise<UpdatePasswordRepository.Output>
}

export namespace UpdatePasswordRepository {
  export type Input = {
    id: number
    password: string
  }
  export type Output = UserModel
}

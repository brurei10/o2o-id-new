import { UserModel } from '@/domain/models'

export interface LoadUserByEmailRepository {
  loadByEmail: (input: LoadUserByEmailRepository.Input) => Promise<LoadUserByEmailRepository.Output>
}

export namespace LoadUserByEmailRepository {
  export type Input = {
    email: string
  }
  export type Output = UserModel
}

import { ProductServiceModel } from '@/domain/models'

export interface AddProductServiceRepository {
  add: (input: AddProductServiceRepository.Input) => Promise<AddProductServiceRepository.Output>
}

export namespace AddProductServiceRepository {
  export type Input = {
    ownerProductId: number
    description: string
    name: string
  }
  export type Output = ProductServiceModel
}

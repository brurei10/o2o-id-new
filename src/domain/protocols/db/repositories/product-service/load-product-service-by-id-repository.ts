import { ProductServiceModel } from '@/domain/models'

export interface LoadProductServiceByIdRepository {
  loadById: (input: LoadProductServiceByIdRepository.Input) => Promise<LoadProductServiceByIdRepository.Output>
}

export namespace LoadProductServiceByIdRepository {
  export type Input = {
    id: number
  }
  export type Output = ProductServiceModel
}

import { FeatureModel, ProfileModel } from '@/domain/models'

export interface AddProfileRepository {
  add: (input: AddProfileRepository.Input) => Promise<AddProfileRepository.Output>
}

export namespace AddProfileRepository {
  export type Input = {
    accountId: number
    name?: string
    productServiceId: number
    features: FeatureModel[]
  }
  export type Output = ProfileModel
}

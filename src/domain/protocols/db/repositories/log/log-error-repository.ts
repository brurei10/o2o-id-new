export interface SaveLogRepository {
  save: (input: SaveLogRepository.Input) => Promise<SaveLogRepository.Output>
}

export namespace SaveLogRepository {
  export type Input = {
    content: string
    type: string
    createdAt?: Date
  }
  export type Output = {
    id: string
    content: string
    type: string
    createdAt: Date
  }
}

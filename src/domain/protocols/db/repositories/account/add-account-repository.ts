import { AccountModel } from '@/domain/models'

export interface AddAccountRepository {
  add: (input: AddAccountRepository.Input) => Promise<AddAccountRepository.Output>
}

export namespace AddAccountRepository {
  export type Input = {
    userOwnerId: number
    productServiceId: number
    contractCode: string
    cpfCnpjOWner?: string
    empresaContrato: string
    filialContrato?: string
    productName?: string
    isActive: boolean
  }
  export type Output = AccountModel
}

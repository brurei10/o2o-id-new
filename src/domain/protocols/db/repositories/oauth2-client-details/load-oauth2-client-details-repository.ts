import { Oauth2ClientDetailsModel } from '@/domain/models'

export interface LoadOauth2ClientDetailsRepository {
  load: (input: LoadOauth2ClientDetailsRepository.Input) => Promise<LoadOauth2ClientDetailsRepository.Output>
}

export namespace LoadOauth2ClientDetailsRepository {
  export type Input = {
    clientId: string
    clientSecret: string
    grantType: string
  }
  export type Output = Oauth2ClientDetailsModel
}

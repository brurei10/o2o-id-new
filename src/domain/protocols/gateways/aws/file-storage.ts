export interface LoadFile {
  load: (input: LoadFile.Input) => Promise<LoadFile.Output>
}

export namespace LoadFile {
  export type Input = { key: string }
  export type Output = any
}

export interface CpfValidation{
  cpfValid: (input: CpfValidation.Input) => Promise<CpfValidation.Output>
}

export namespace CpfValidation{
  export type Input = {
    value: string
  }
  export type Output = boolean
}

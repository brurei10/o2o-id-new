export interface CnpjValidation{
  cnpjValid: (input: CnpjValidation.Input) => Promise<CnpjValidation.Output>
}

export namespace CnpjValidation{
  export type Input = {
    value: string
  }
  export type Output = boolean
}

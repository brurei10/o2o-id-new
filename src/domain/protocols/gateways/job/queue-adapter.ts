export interface QueueAdapter {
  add: (input: QueueAdapter.Input) => Promise<void>
}

export namespace QueueAdapter {
  export type Input = any
}

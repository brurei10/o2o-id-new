export interface Encrypter{
  encrypt: (input: Encrypter.Input) => Promise<Encrypter.Output>
}

export namespace Encrypter{
  export type Input = {
    value: object | string | number
  }
  export type Output = {
    refresh_token: string
    access_token: string
    expires_in: number
  }
}

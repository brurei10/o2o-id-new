export interface MailProvider {
  sendMail: (input: MailProvider.Input) => Promise<MailProvider.Output>
}

export namespace MailProvider {
  export type Input = {
    from: string
    to: string
    subject: string
    viewPath: string
    data: any
  }
  export type Output = any
}

export interface Validation<T>{
  validate: (input: T) => Promise<void>
}

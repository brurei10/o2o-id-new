import { HttpError } from '@/application/errors'

export class BaseValidation {
  protected notify (error: HttpError): void {
    throw error
  }
}


import { DbAddUserValidation } from './add-user-validation'
import { CnpjValidation, CpfValidation, LoadFeatureByIdRepository, LoadProductServiceByIdRepository, LoadUserByCpfCnpjRepository, LoadUserByEmailRepository } from '@/domain/protocols'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockFeatureModel, mockProductServiceModel, mockUserInput, mockUserModel } from '@/domain/test'
import { AuthError } from '@/domain/enums'
import { cnpj, cpf } from 'cpf-cnpj-validator'

describe('DbAddUser Validation', () => {
  let productServiceRepository: MockProxy<LoadProductServiceByIdRepository>
  let featureRepository: MockProxy<LoadFeatureByIdRepository>
  let cpfCnpjValidation: MockProxy<CpfValidation & CnpjValidation>
  let userRepository: MockProxy<LoadUserByCpfCnpjRepository & LoadUserByEmailRepository>
  let sut: DbAddUserValidation

  const product = mockProductServiceModel()
  const feature = mockFeatureModel()
  const user = mockUserModel()

  const params = { ...mockUserInput(), cpfCnpj: cpf.generate() }

  beforeAll(() => {
    productServiceRepository = mock()
    featureRepository = mock()
    cpfCnpjValidation = mock()
    userRepository = mock()
    productServiceRepository.loadById.mockResolvedValue(product)
    userRepository.loadByCpfCnpj.mockResolvedValue(null)
    userRepository.loadByEmail.mockResolvedValue(null)
    featureRepository.loadById.mockResolvedValue(feature)
    cpfCnpjValidation.cnpjValid.mockResolvedValue(true)
    cpfCnpjValidation.cpfValid.mockResolvedValue(true)
  })

  beforeEach(() => {
    sut = new DbAddUserValidation(productServiceRepository, featureRepository, cpfCnpjValidation, userRepository)
  })

  it('should call LoadProductServiceByIdRepository with correct id', async () => {
    await sut.validate(params)

    expect(productServiceRepository.loadById).toHaveBeenCalledWith({ id: params.product.id })
    expect(productServiceRepository.loadById).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadProductServiceByIdRepository throws', async () => {
    productServiceRepository.loadById.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadFeatureByIdRepository with correct id', async () => {
    await sut.validate(params)

    expect(featureRepository.loadById).toHaveBeenCalledWith({ id: params.product.accounts[0].profiles[0].features[0].id })
    expect(featureRepository.loadById).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadFeatureByIdRepository throws', async () => {
    featureRepository.loadById.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadUserByCpfCnpjRepository with correct cpf/cnpj', async () => {
    await sut.validate(params)

    expect(userRepository.loadByCpfCnpj).toHaveBeenCalledWith({ cpfCnpj: params.cpfCnpj })
    expect(userRepository.loadByCpfCnpj).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByCpfCnpjRepository throws', async () => {
    userRepository.loadByCpfCnpj.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(userRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(userRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    userRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call CpfValidation with correct value', async () => {
    await sut.validate(params)

    expect(cpfCnpjValidation.cpfValid).toHaveBeenCalledWith({ value: params.cpfCnpj })
    expect(cpfCnpjValidation.cpfValid).toHaveBeenCalledTimes(1)
  })

  it('should throw if CpfValidation throws', async () => {
    cpfCnpjValidation.cpfValid.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call CnpjValidation with correct value', async () => {
    const cpfCnpj = cnpj.generate()
    await sut.validate({ ...params, cpfCnpj })

    expect(cpfCnpjValidation.cnpjValid).toHaveBeenCalledWith({ value: cpfCnpj })
    expect(cpfCnpjValidation.cnpjValid).toHaveBeenCalledTimes(1)
  })

  it('should throw if CnpjValidation throws', async () => {
    const cpfCnpj = cnpj.generate()
    cpfCnpjValidation.cnpjValid.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate({ ...params, cpfCnpj })
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should throw an HttpError if cpf invalid', async () => {
    cpfCnpjValidation.cpfValid.mockResolvedValueOnce(false)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.CpfInvalid))
  })

  it('should throw an HttpError if cnpj invalid', async () => {
    const cpfCnpj = cnpj.generate()
    cpfCnpjValidation.cnpjValid.mockResolvedValueOnce(false)

    const promise = sut.validate({ ...params, cpfCnpj })
    await expect(promise).rejects.toThrow(new Error(AuthError.CnpjInvalid))
  })

  it('should throw an HttpError if product not exists', async () => {
    productServiceRepository.loadById.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.ProductNotFound))
  })

  it('should throw an HttpError if feature not exists', async () => {
    featureRepository.loadById.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(`Feature ${params.product.accounts[0].profiles[0].features[0].id} não encontrada`))
  })

  it('should throw an HttpError if user exists', async () => {
    userRepository.loadByCpfCnpj.mockResolvedValueOnce(user)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.UserCpfCnpjExist))
  })

  it('should throw an HttpError if user exists', async () => {
    userRepository.loadByEmail.mockResolvedValueOnce(user)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.UserEmailExist))
  })
})

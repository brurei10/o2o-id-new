import { AuthError, HttpStatusCode } from '@/domain/enums'
import { AddUser } from '@/domain/usecases'
import { BaseValidation, Validation } from '@/domain/validation'
import { HttpError } from '@/application/errors'
import { CnpjValidation, CpfValidation, LoadFeatureByIdRepository, LoadProductServiceByIdRepository, LoadUserByCpfCnpjRepository, LoadUserByEmailRepository } from '@/domain/protocols'

export class DbAddUserValidation extends BaseValidation implements Validation<AddUser.Input> {
  constructor (
    private readonly productServiceRepository: LoadProductServiceByIdRepository,
    private readonly featureRepository: LoadFeatureByIdRepository,
    private readonly cpfCnpjValidation: CpfValidation & CnpjValidation,
    private readonly userRepository: LoadUserByCpfCnpjRepository & LoadUserByEmailRepository
  ) {
    super()
  }

  async validate ({ product: { id, accounts }, cpfCnpj, email }: AddUser.Input): Promise<void> {
    await this.productExist({ id })
    await this.featureExist({ accounts })
    await this.isCpfCnpjValid({ cpfCnpj })
    await this.userCpfCnpjExists({ cpfCnpj })
    await this.userEmailExists({ email })
  }

  private async productExist ({ id }: {id: number}): Promise<void> {
    const result = await this.productServiceRepository.loadById({ id })
    if (!result) {
      this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.ProductNotFound))
    }
  }

  private async featureExist ({ accounts }: {accounts: AddUser.Account[]}): Promise<void> {
    for (const acount of accounts) {
      for (const profile of acount.profiles) {
        for (const feature of profile.features) {
          const result = await this.featureRepository.loadById({ id: feature.id })
          if (!result) {
            this.notify(new HttpError(HttpStatusCode.badRequest, `Feature ${feature.id} não encontrada`))
          }
        }
      }
    }
  }

  private async isCpfCnpjValid ({ cpfCnpj }: {cpfCnpj: string}): Promise<void> {
    const str = cpfCnpj.replace(/[^\d]+/g,'')
    if (str.length === 11) {
      const isValid = await this.cpfCnpjValidation.cpfValid({ value: str })
      if (!isValid) {
        this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.CpfInvalid))
      }
    } else if (str.length === 14) {
      const isValid = await this.cpfCnpjValidation.cnpjValid({ value: str })
      if (!isValid) {
        this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.CnpjInvalid))
      }
    } else {
      this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.CpfCnpjInvalid))
    }
  }

  private async userCpfCnpjExists ({ cpfCnpj }: {cpfCnpj: string}): Promise<void> {
    const result = await this.userRepository.loadByCpfCnpj({ cpfCnpj })
    if (result) {
      this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.UserCpfCnpjExist))
    }
  }

  private async userEmailExists ({ email }: {email: string}): Promise<void> {
    const result = await this.userRepository.loadByEmail({ email })
    if (result) {
      this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.UserEmailExist))
    }
  }
}

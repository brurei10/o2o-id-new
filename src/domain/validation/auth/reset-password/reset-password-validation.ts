import { LoadUserByEmailRepository } from '@/domain/protocols'
import { UserModel } from '@/domain/models'
import { ResetPassword } from '@/domain/usecases'
import { BaseValidation, Validation } from '@/domain/validation'
import { AuthError, HttpStatusCode } from '@/domain/enums'
import { HttpError } from '@/application/errors'

export class ResetPasswordValidation extends BaseValidation implements Validation<ResetPassword.Input> {
  constructor (
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository
  ) {
    super()
  }

  async validate ({ email }: {email: string}): Promise<void> {
    const user = await this.loadUserByEmailRepository.loadByEmail({ email })
    await this.userExist({ user })
  }

  private async userExist ({ user }: {user: UserModel}): Promise<void> {
    if (!user) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.EmailNotFound))
    }
  }
}

import { LoadUserByEmailRepository } from '@/domain/protocols'
import { ResetPassword } from '@/domain/usecases'

import { Validation } from '@/domain/validation'
import { AuthError } from '@/domain/enums'
import { mock, MockProxy } from 'jest-mock-extended'
import { mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'
import { ResetPasswordValidation } from './reset-password-validation'

describe('ResetPasswordValidation', () => {
  let sut: Validation<ResetPassword.Input>
  let params: ResetPassword.Input
  let userModel: LoadUserByEmailRepository.Output
  let loadUserByEmailRepository: MockProxy<LoadUserByEmailRepository>

  beforeAll(() => {
    params = {
      email: faker.internet.email()
    }
    userModel = mockUserModel()
    loadUserByEmailRepository = mock()
    loadUserByEmailRepository.loadByEmail.mockResolvedValue(userModel)
  })

  beforeEach(() => {
    sut = new ResetPasswordValidation(loadUserByEmailRepository)
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    loadUserByEmailRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should throw an HttpError if user not exists', async () => {
    loadUserByEmailRepository.loadByEmail.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.EmailNotFound))
  })
})

import { LoadAccessCodeByUserEmailRepository, LoadUserByEmailRepository } from '@/domain/protocols'
import { ConfirmResetPasswordValidation } from './confirm-reset-password-validation'
import { ConfirmResetPassword } from '@/domain/usecases'

import { Validation } from '@/domain/validation'
import { AuthError } from '@/domain/enums'
import { mock, MockProxy } from 'jest-mock-extended'
import { faker } from '@faker-js/faker'
import { mockAccessCodeModel, mockUserModel } from '@/domain/test'
import moment from 'moment'

describe('DbConfirmResetPasswordValidation', () => {
  let sut: Validation<ConfirmResetPassword.Input>
  let params: ConfirmResetPassword.Input
  let userModel: LoadUserByEmailRepository.Output
  let accessCodeModel: LoadAccessCodeByUserEmailRepository.Output
  let accessCodeRepository: MockProxy<LoadAccessCodeByUserEmailRepository>
  let loadUserByEmailRepository: MockProxy<LoadUserByEmailRepository>

  beforeAll(() => {
    accessCodeModel = { ...mockAccessCodeModel(), validAt: moment().add(4,'hours').toDate() }

    params = {
      email: faker.internet.email(),
      code: accessCodeModel.code
    }
    userModel = mockUserModel()
    accessCodeRepository = mock()
    accessCodeRepository.loadByUser.mockResolvedValue(accessCodeModel)
    loadUserByEmailRepository = mock()
    loadUserByEmailRepository.loadByEmail.mockResolvedValue(userModel)
  })

  beforeEach(() => {
    sut = new ConfirmResetPasswordValidation(accessCodeRepository, loadUserByEmailRepository)
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    loadUserByEmailRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadAccessCodeByUserEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(accessCodeRepository.loadByUser).toHaveBeenCalledWith({ email: params.email })
    expect(accessCodeRepository.loadByUser).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadAccessCodeByUserEmailRepository throws', async () => {
    accessCodeRepository.loadByUser.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should throw an HttpError if user not exists', async () => {
    loadUserByEmailRepository.loadByEmail.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.EmailNotFound))
  })

  it('should throw an HttpError if access code not exists', async () => {
    accessCodeRepository.loadByUser.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.InvalidCode))
  })

  it('should throw an HttpError if access code expired', async () => {
    accessCodeRepository.loadByUser.mockResolvedValueOnce({ ...accessCodeModel, validAt: moment().subtract(4,'hours').toDate() })

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.CodeExpired))
  })

  it('should throw an HttpError if access code invalid', async () => {
    accessCodeRepository.loadByUser.mockResolvedValueOnce({ ...accessCodeModel, code: faker.random.alphaNumeric(10) })

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.InvalidCode))
  })
})

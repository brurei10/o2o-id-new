import { LoadAccessCodeByUserEmailRepository, LoadUserByEmailRepository } from '@/domain/protocols'
import { UserModel } from '@/domain/models'
import { ConfirmResetPassword } from '@/domain/usecases'
import { BaseValidation, Validation } from '@/domain/validation'
import { AuthError, HttpStatusCode } from '@/domain/enums'
import { HttpError } from '@/application/errors'

export class ConfirmResetPasswordValidation extends BaseValidation implements Validation<ConfirmResetPassword.Input> {
  constructor (
    private readonly accessCodeRepository: LoadAccessCodeByUserEmailRepository,
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository
  ) {
    super()
  }

  async validate ({ email, code }: {email: string, code: string}): Promise<void> {
    const user = await this.loadUserByEmailRepository.loadByEmail({ email })
    const accessCode = await this.accessCodeRepository.loadByUser({ email })
    await this.userExist({ user })
    await this.validCode({ code,accessCode })
    await this.validDate({ accessCode })
  }

  private async userExist ({ user }: {user: UserModel}): Promise<void> {
    if (!user) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.EmailNotFound))
    }
  }

  private async validCode ({ code, accessCode }: {code: string , accessCode: LoadAccessCodeByUserEmailRepository.Output}): Promise<void> {
    if (!accessCode) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.InvalidCode))
    }
    if (code !== accessCode.code) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.InvalidCode))
    }
  }

  private async validDate ({ accessCode }): Promise<void> {
    if ((new Date()).valueOf() >= (new Date(accessCode.validAt)).valueOf()) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.CodeExpired))
    }
  }
}

import { UserModel } from '@/domain/models'
import { Authentication } from '@/domain/usecases'
import { BaseValidation, Validation } from '@/domain/validation'
import { AuthError, HttpStatusCode } from '@/domain/enums'
import { HttpError } from '@/application/errors'
import { HashComparer, LoadProductServiceByIdRepository, LoadUserByCredentialsRepository } from '@/domain/protocols'

export class AuthValidation extends BaseValidation implements Validation<Authentication.Input> {
  constructor (
    private readonly productServiceRepository: LoadProductServiceByIdRepository,
    private readonly userRepository: LoadUserByCredentialsRepository,
    private readonly hashComparer: HashComparer
  ) {
    super()
  }

  async validate ({ password, login, product }: Authentication.Input): Promise<void> {
    const user = await this.userRepository.loadByCredentials({ login })
    await this.productExist({ id: product })
    await this.userExist({ user })
    await this.isActive({ user })
    await this.validPassword({ hash: user.password, password })
  }

  private async productExist ({ id }: {id: number}): Promise<void> {
    const result = await this.productServiceRepository.loadById({ id })
    if (!result) {
      this.notify(new HttpError(HttpStatusCode.badRequest, AuthError.ProductNotFound))
    }
  }

  private async userExist ({ user }: {user: UserModel}): Promise<void> {
    if (!user) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.EmailNotFound))
    }
  }

  private async isActive ({ user }: {user: UserModel}): Promise<void> {
    if (!user.active) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.UserDisabled))
    }
  }

  private async validPassword ({ hash, password }: {hash: string, password: string}): Promise<void> {
    const isValid = await this.hashComparer.compare({ value: password, hash })
    if (!isValid) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.InvalidPassword))
    }
  }
}

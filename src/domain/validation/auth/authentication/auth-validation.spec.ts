import { AuthValidation } from './auth-validation'
import { HashComparer, LoadProductServiceByIdRepository, LoadUserByCredentialsRepository } from '@/domain/protocols'
import { Authentication } from '@/domain/usecases'

import { Validation } from '@/domain/validation'
import { AuthError } from '@/domain/enums'
import { mock, MockProxy } from 'jest-mock-extended'
import { mockProductServiceModel, mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('AuthValidation', () => {
  let loadUserByCredentialsRepository: MockProxy<LoadUserByCredentialsRepository>
  let productServiceRepository: MockProxy<LoadProductServiceByIdRepository>
  let hashComparer: MockProxy<HashComparer>
  let sut: Validation<Authentication.Input>
  let params: Authentication.Input
  let model: LoadUserByCredentialsRepository.Output
  let product: LoadProductServiceByIdRepository.Output

  beforeAll(() => {
    params = {
      login: faker.internet.email(),
      password: faker.internet.password(),
      clientId: faker.internet.avatar(),
      clientSecret: faker.internet.password(),
      product: faker.datatype.number(),
      grantType: 'password'
    }
    product = mockProductServiceModel()
    model = { ...mockUserModel(), active: true }
    productServiceRepository = mock()
    productServiceRepository.loadById.mockResolvedValue(product)
    loadUserByCredentialsRepository = mock()
    loadUserByCredentialsRepository.loadByCredentials.mockResolvedValue(model)
    hashComparer = mock()
    hashComparer.compare.mockResolvedValue(true)
  })

  beforeEach(() => {
    sut = new AuthValidation(productServiceRepository, loadUserByCredentialsRepository, hashComparer)
  })

  it('should call LoadProductServiceByIdRepository with correct id', async () => {
    await sut.validate(params)

    expect(productServiceRepository.loadById).toHaveBeenCalledWith({ id: params.product })
    expect(productServiceRepository.loadById).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadProductServiceByIdRepository throws', async () => {
    productServiceRepository.loadById.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(loadUserByCredentialsRepository.loadByCredentials).toHaveBeenCalledWith({ login: params.login })
    expect(loadUserByCredentialsRepository.loadByCredentials).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    loadUserByCredentialsRepository.loadByCredentials.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should throw an HttpError if user not exists', async () => {
    loadUserByCredentialsRepository.loadByCredentials.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.EmailNotFound))
  })

  it('should throw an HttpError if user not enabled', async () => {
    loadUserByCredentialsRepository.loadByCredentials.mockResolvedValueOnce({ ...model, active: false })

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.UserDisabled))
  })

  it('should throw an HttpError if product not exists', async () => {
    productServiceRepository.loadById.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.ProductNotFound))
  })

  it('should call HashComparer with correct values', async () => {
    await sut.validate(params)

    expect(hashComparer.compare).toHaveBeenCalledWith({ value: params.password, hash: model.password })
    expect(hashComparer.compare).toHaveBeenCalledTimes(1)
  })

  it('should throw if HashComparer throws', async () => {
    hashComparer.compare.mockRejectedValueOnce(new Error('hash_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('hash_error'))
  })

  it('should throw an HttpError if password is not valid', async () => {
    hashComparer.compare.mockResolvedValueOnce(false)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.InvalidPassword))
  })
})

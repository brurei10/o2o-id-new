import { LoadUserByEmailRepository, Decrypter } from '@/domain/protocols'
import { UserModel } from '@/domain/models'
import { UpdatePassword } from '@/domain/usecases'
import { BaseValidation, Validation } from '@/domain/validation'
import { AuthError, HttpStatusCode } from '@/domain/enums'
import { HttpError } from '@/application/errors'

export class UpdatePasswordValidation extends BaseValidation implements Validation<UpdatePassword.Input> {
  constructor (
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository,
    private readonly decrypter: Decrypter
  ) {
    super()
  }

  async validate ({ email, resetToken }: { email: string, resetToken: string }): Promise<void> {
    const user = await this.loadUserByEmailRepository.loadByEmail({ email })
    await this.userExist({ user })
    const value = await this.decrypter.decrypt({ value: resetToken })
    await this.validValue({ value })
  }

  private async userExist ({ user }: { user: UserModel }): Promise<void> {
    if (!user) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.EmailNotFound))
    }
  }

  private async validValue ({ value }: { value: string }): Promise<void> {
    if (!value) {
      this.notify(new HttpError(HttpStatusCode.unauthorized, AuthError.InvalidToken))
    }
  }
}

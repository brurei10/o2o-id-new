import { LoadUserByEmailRepository,Decrypter } from '@/domain/protocols'
import { UpdatePassword } from '@/domain/usecases'
import { Validation } from '@/domain/validation'
import { AuthError } from '@/domain/enums'
import { mock, MockProxy } from 'jest-mock-extended'
import { mockUserModel } from '@/domain/test'
import { UpdatePasswordValidation } from './update-password-validation'
import { faker } from '@faker-js/faker'

describe('UpdatePasswordValidation', () => {
  let sut: Validation<UpdatePassword.Input>
  let params: UpdatePassword.Input
  let userModel: LoadUserByEmailRepository.Output
  let decrypter: MockProxy<Decrypter>
  let loadUserByEmailRepository: MockProxy<LoadUserByEmailRepository>

  beforeAll(() => {
    params = {
      email: faker.internet.email(),
      password: faker.random.alphaNumeric(15),
      resetToken: faker.random.alphaNumeric(15)
    }
    userModel = mockUserModel()
    decrypter = mock()
    decrypter.decrypt.mockResolvedValue(faker.random.alphaNumeric(10))
    loadUserByEmailRepository = mock()
    loadUserByEmailRepository.loadByEmail.mockResolvedValue(userModel)
  })

  beforeEach(() => {
    sut = new UpdatePasswordValidation(loadUserByEmailRepository, decrypter)
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.validate(params)

    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    loadUserByEmailRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })
  it('should call Decrypter with correct params', async () => {
    await sut.validate(params)

    expect(decrypter.decrypt).toHaveBeenCalledWith({ value: params.resetToken })
    expect(decrypter.decrypt).toHaveBeenCalledTimes(1)
  })

  it('should throw if Decrypter throw', async () => {
    decrypter.decrypt.mockRejectedValueOnce(new Error('decrypt_error'))
    const promise = sut.validate(params)

    await expect(promise).rejects.toThrow(new Error('decrypt_error'))
  })

  it('should throw an HttpError if user not exists', async () => {
    loadUserByEmailRepository.loadByEmail.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.EmailNotFound))
  })

  it('should throw an HttpError if code not valid', async () => {
    decrypter.decrypt.mockResolvedValueOnce(null)

    const promise = sut.validate(params)
    await expect(promise).rejects.toThrow(new Error(AuthError.InvalidToken))
  })
})

import { MailProvider } from '@/domain/protocols'

import { mock, MockProxy } from 'jest-mock-extended'
import { SendMailService } from './send-mail-service'
import { faker } from '@faker-js/faker'
import { SendMail } from '@/domain/usecases'

describe('SendMailService', () => {
  let mailProvider: MockProxy<MailProvider>
  let sut: SendMailService
  let input: SendMail.Input

  beforeAll(() => {
    input = {
      from: faker.internet.email(),
      to: faker.internet.email(),
      subject: faker.random.words(),
      viewPath: faker.internet.url(),
      data: faker.datatype.json()
    }
    mailProvider = mock()
  })

  beforeEach(() => {
    sut = new SendMailService(mailProvider)
  })

  it('should call MailProvider with correct params', async () => {
    await sut.send(input)

    expect(mailProvider.sendMail).toHaveBeenCalledWith(input)
    expect(mailProvider.sendMail).toHaveBeenCalledTimes(1)
  })

  it('should call SaveLogRepository with correct stack', async () => {
    mailProvider.sendMail.mockRejectedValueOnce(new Error('send_mail_error'))

    const promise = sut.send(input)

    await expect(promise).rejects.toThrow(new Error('send_mail_error'))
  })
})

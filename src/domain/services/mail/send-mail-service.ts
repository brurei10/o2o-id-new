import { MailProvider } from '@/domain/protocols'
import { SendMail } from '@/domain/usecases'

export class SendMailService implements SendMail {
  constructor (
    private readonly mailProvider: MailProvider
  ) { }

  async send (params: SendMail.Input): Promise<SendMail.Output> {
    return await this.mailProvider.sendMail(params)
  }
}

import { UpdatePassword } from '@/domain/usecases'
import { UpdatePasswordService } from './update-password-service'
import { Hasher, LoadUserByEmailRepository, UpdatePasswordRepository } from '@/domain/protocols'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('UpdatePasswordService', () => {
  let userRepository: MockProxy<LoadUserByEmailRepository & UpdatePasswordRepository>
  let hasher: MockProxy<Hasher>
  let sut: UpdatePasswordService
  let params: UpdatePassword.Input
  let model: LoadUserByEmailRepository.Output
  let hash: string

  beforeAll(() => {
    hash = faker.internet.password()

    model = mockUserModel()
    params = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      resetToken: faker.datatype.uuid()
    }
    userRepository = mock()
    userRepository.loadByEmail.mockResolvedValue(model)
    hasher = mock()
    hasher.hash.mockResolvedValue(hash)
  })

  beforeEach(() => {
    sut = new UpdatePasswordService(userRepository, hasher)
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.reset(params)

    expect(userRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(userRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByEmailRepository throws', async () => {
    userRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.reset(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call hasher with correct password', async () => {
    await sut.reset(params)

    expect(hasher.hash).toHaveBeenCalledWith({ value: params.password })
    expect(hasher.hash).toHaveBeenCalledTimes(1)
  })

  it('should throw if Hasher throws', async () => {
    hasher.hash.mockRejectedValueOnce(new Error('hash_error'))
    const promise = sut.reset(params)

    await expect(promise).rejects.toThrow(new Error('hash_error'))
  })

  it('Should call UpdatePasswordRepository with correct values', async () => {
    await sut.reset(params)

    expect(userRepository.updatePassword).toHaveBeenCalledWith({ id: model.id, password: hash })
  })

  it('Should throw if UpdatePasswordRepository throws', async () => {
    userRepository.updatePassword.mockRejectedValueOnce(new Error('update_error'))
    const promise = sut.reset(params)

    await expect(promise).rejects.toThrow(new Error('update_error'))
  })
})

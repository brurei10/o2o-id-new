import { Hasher, LoadUserByEmailRepository, UpdatePasswordRepository } from '@/domain/protocols'
import { UpdatePassword } from '@/domain/usecases'

export class UpdatePasswordService implements UpdatePassword {
  constructor (
    private readonly userRepository: LoadUserByEmailRepository & UpdatePasswordRepository,
    private readonly hasher: Hasher
  ) { }

  async reset ({ email, password }: UpdatePassword.Input): Promise<void> {
    const user = await this.userRepository.loadByEmail({ email })
    const hash = await this.hasher.hash({ value: password })
    await this.userRepository.updatePassword({ id: user.id, password: hash })
  }
}

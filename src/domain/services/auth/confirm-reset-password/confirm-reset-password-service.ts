import { Encrypter, LoadAccessCodeByUserEmailRepository, DeleteAccessCodeRepository } from '@/domain/protocols'
import { ConfirmResetPassword } from '@/domain/usecases'

export class ConfirmResetPasswordService implements ConfirmResetPassword {
  constructor (
    private readonly accessCodeRepository: LoadAccessCodeByUserEmailRepository & DeleteAccessCodeRepository,
    private readonly encrypter: Encrypter
  ) { }

  async confirm ({ email, code }: ConfirmResetPassword.Input): Promise<ConfirmResetPassword.Output> {
    const accessCode = await this.accessCodeRepository.loadByUser({ email })
    if (accessCode) {
      if (code === accessCode.code) {
        if ((new Date()).valueOf() < (new Date(accessCode.validAt)).valueOf()) {
          const result = await this.encrypter.encrypt({ value: accessCode.userId })
          await this.accessCodeRepository.delete({ id: accessCode.id })
          return { resetToken: result.access_token, email }
        }
      }
    }
    return null
  }
}

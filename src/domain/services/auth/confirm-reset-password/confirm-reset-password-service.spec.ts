
import { ConfirmResetPasswordService } from './confirm-reset-password-service'
import { Encrypter, LoadAccessCodeByUserEmailRepository, DeleteAccessCodeRepository } from '@/domain/protocols'
import { ConfirmResetPassword } from '@/domain/usecases'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockAccessCodeModel } from '@/domain/test'
import { faker } from '@faker-js/faker'
import Mockdate from 'mockdate'

describe('ConfirmResetPasswordService Usecase', () => {
  let accessCodeRepository: MockProxy<LoadAccessCodeByUserEmailRepository & DeleteAccessCodeRepository>
  let encrypter: MockProxy<Encrypter>
  let sut: ConfirmResetPasswordService
  let model: LoadAccessCodeByUserEmailRepository.Output
  let code: string
  let validAt: Date
  let auth: Encrypter.Output
  let params: ConfirmResetPassword.Input

  beforeAll(() => {
    Mockdate.set(new Date())
    const today = new Date()
    validAt = new Date(today.setHours(today.getHours() + 4))
    auth = {
      refresh_token: faker.datatype.uuid(),
      access_token: faker.datatype.uuid(),
      expires_in: faker.datatype.number()
    }
    params = {
      email: faker.internet.email(),
      code: faker.random.alphaNumeric()
    }
    code = params.code
    model = { ...mockAccessCodeModel(), validAt, code }
    accessCodeRepository = mock()
    accessCodeRepository.loadByUser.mockResolvedValue(model)
    encrypter = mock()
    encrypter.encrypt.mockResolvedValue(auth)
  })

  beforeEach(() => {
    sut = new ConfirmResetPasswordService(accessCodeRepository, encrypter)
  })

  afterAll(() => {
    Mockdate.reset()
  })

  it('should call LoadUserAccessByUserEmailRepository', async () => {
    await sut.confirm(params)

    expect(accessCodeRepository.loadByUser).toHaveBeenCalledWith({ email: params.email })
    expect(accessCodeRepository.loadByUser).toHaveBeenCalledTimes(1)
  })

  it('Should throw if LoadUserAccessByUserEmailRepository throws', async () => {
    accessCodeRepository.loadByUser.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.confirm(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call Encrypter with correct value', async () => {
    await sut.confirm(params)

    expect(encrypter.encrypt).toHaveBeenCalledWith({ value: model.userId })
    expect(encrypter.encrypt).toHaveBeenCalledTimes(1)
  })

  it('should throw if Encrypter throws', async () => {
    encrypter.encrypt.mockRejectedValueOnce(new Error('encrypt_error'))
    const promise = sut.confirm(params)

    await expect(promise).rejects.toThrow(new Error('encrypt_error'))
  })

  it('should call SaveUserAcccessRepository with correc values', async () => {
    await sut.confirm(params)

    expect(accessCodeRepository.delete).toHaveBeenCalledWith({ id: model.id })
    expect(accessCodeRepository.delete).toHaveBeenCalledTimes(1)
  })

  it('Should throw if SaveUserAcccessRepository throws', async () => {
    accessCodeRepository.delete.mockRejectedValueOnce(new Error('save_error'))

    const promise = sut.confirm(params)
    await expect(promise).rejects.toThrow(new Error('save_error'))
  })

  it('should return an access token on success', async () => {
    const access = await sut.confirm(params)

    expect(access).toEqual({ resetToken: auth.access_token, email: params.email })
  })
})

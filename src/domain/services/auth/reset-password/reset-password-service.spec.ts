
import { ResetPasswordService } from './reset-password-service'
import { CodeGenerator, LoadAccessCodeByUserEmailRepository, LoadUserByEmailRepository, QueueAdapter, SaveAccessCodeRepository } from '@/domain/protocols'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockAccessCodeModel, mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'
import Mockdate from 'mockdate'

describe('ResetPasswordService Usecase', () => {
  let loadUserByEmailRepository: MockProxy<LoadUserByEmailRepository>
  let queueAdapter: MockProxy<QueueAdapter>
  let codeGenerator: MockProxy<CodeGenerator>
  let accessCodeRepository: MockProxy<LoadAccessCodeByUserEmailRepository & SaveAccessCodeRepository>
  let sut: ResetPasswordService
  let code: string
  let validAt: Date

  const user = mockUserModel()
  const userAccessModel = mockAccessCodeModel()

  const params = {
    email: faker.internet.email()
  }

  beforeAll(() => {
    Mockdate.set(new Date())
    const today = new Date()
    validAt = new Date(today.setHours(today.getHours() + 4))
    code = faker.random.alphaNumeric()
    loadUserByEmailRepository = mock()
    codeGenerator = mock()
    codeGenerator.generate.mockResolvedValue(code)
    accessCodeRepository = mock()
    accessCodeRepository.loadByUser.mockResolvedValue(userAccessModel)
    queueAdapter = mock()
    loadUserByEmailRepository.loadByEmail.mockResolvedValue(user)
  })

  beforeEach(() => {
    sut = new ResetPasswordService(loadUserByEmailRepository, codeGenerator, accessCodeRepository, queueAdapter)
  })

  afterAll(() => {
    Mockdate.reset()
  })

  it('should call LoadUserByEmailRepository with correct email', async () => {
    await sut.send(params)

    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledWith({ email: params.email })
    expect(loadUserByEmailRepository.loadByEmail).toHaveBeenCalledTimes(1)
  })

  it('Should throw if LoadUserByEmailRepository throws', async () => {
    loadUserByEmailRepository.loadByEmail.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.send(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call CodeGenerator', async () => {
    await sut.send(params)

    expect(codeGenerator.generate).toHaveBeenCalled()
    expect(codeGenerator.generate).toHaveBeenCalledTimes(1)
  })

  it('Should throw if CodeGenerator throws', async () => {
    codeGenerator.generate.mockRejectedValueOnce(new Error('generate_error'))

    const promise = sut.send(params)
    await expect(promise).rejects.toThrow(new Error('generate_error'))
  })

  it('should call LoadUserAccessByUserEmailRepository', async () => {
    await sut.send(params)

    expect(accessCodeRepository.loadByUser).toHaveBeenCalledWith({ email: params.email })
    expect(accessCodeRepository.loadByUser).toHaveBeenCalledTimes(1)
  })

  it('Should throw if LoadUserAccessByUserEmailRepository throws', async () => {
    accessCodeRepository.loadByUser.mockRejectedValueOnce(new Error('load_error'))

    const promise = sut.send(params)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call SaveUserAcccessRepository', async () => {
    await sut.send(params)

    expect(accessCodeRepository.save).toHaveBeenCalledWith({
      ...userAccessModel,
      code,
      validAt,
      type: 'reset_password',
      userId: user.id
    })
    expect(accessCodeRepository.save).toHaveBeenCalledTimes(1)
  })

  it('Should throw if SaveUserAcccessRepository throws', async () => {
    accessCodeRepository.save.mockRejectedValueOnce(new Error('save_error'))

    const promise = sut.send(params)
    await expect(promise).rejects.toThrow(new Error('save_error'))
  })

  it('should call QueueAdapter with correct params', async () => {
    await sut.send(params)

    expect(queueAdapter.add).toHaveBeenCalledWith({
      from: process.env.MAIL_SENDER,
      to: user.email,
      subject: 'o2o - Redefinição de senha',
      viewPath: 'user-reset-password',
      data: {
        code
      }
    })
    expect(queueAdapter.add).toHaveBeenCalledTimes(1)
  })

  it('should throw if QueueAdapter throws', async () => {
    queueAdapter.add.mockRejectedValueOnce(new Error('send_mail_error'))
    const promise = sut.send(params)

    await expect(promise).rejects.toThrow()
  })
})

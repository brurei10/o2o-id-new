import { CodeGenerator, LoadAccessCodeByUserEmailRepository, LoadUserByEmailRepository, QueueAdapter, SaveAccessCodeRepository } from '@/domain/protocols'
import { ResetPassword } from '@/domain/usecases'

export class ResetPasswordService implements ResetPassword {
  constructor (
    private readonly loadUserByEmailRepository: LoadUserByEmailRepository,
    private readonly codeGenerator: CodeGenerator,
    private readonly accessCodeRepository: LoadAccessCodeByUserEmailRepository & SaveAccessCodeRepository,
    private readonly queueAdapter: QueueAdapter
  ) { }

  async send ({ email }: ResetPassword.Input): Promise<void> {
    const user = await this.loadUserByEmailRepository.loadByEmail({ email })
    const code = await this.codeGenerator.generate()
    const today = new Date()
    const validAt = new Date(today.setHours(today.getHours() + 4))
    const userAccess = await this.accessCodeRepository.loadByUser({ email })
    await this.accessCodeRepository.save({
      ...userAccess,
      code,
      type: 'reset_password',
      validAt,
      userId: user.id
    })
    await this.queueAdapter.add({
      from: process.env.MAIL_SENDER,
      to: user.email,
      subject: 'o2o - Redefinição de senha',
      viewPath: 'user-reset-password',
      data: {
        code
      }
    })
  }
}

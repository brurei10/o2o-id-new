import { Encrypter, LoadOauth2ClientDetailsRepository, LoadUserByCredentialsRepository } from '@/domain/protocols'
import { Authentication } from '@/domain/usecases'

export class AuthenticationService implements Authentication {
  constructor (
    private readonly userRepository: LoadUserByCredentialsRepository,
    private readonly oauth2ClientDetailsRepository: LoadOauth2ClientDetailsRepository,
    private readonly encrypter: Encrypter
  ) {}

  async auth ({ login, clientId, clientSecret, grantType }: Authentication.Input): Promise<Authentication.Output> {
    const oauth = await this.oauth2ClientDetailsRepository.load({ clientId, clientSecret, grantType })
    if (oauth) {
      const user = await this.userRepository.loadByCredentials({ login })
      const auth = await this.encrypter.encrypt({ value: user })
      return { ...auth, scope: oauth.scopes, user: { ...user, password: undefined } }
    }
  }
}

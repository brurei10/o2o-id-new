import { AuthenticationService } from './authentication-service'
import { Encrypter, LoadOauth2ClientDetailsRepository, LoadUserByCredentialsRepository } from '@/domain/protocols'
import { Authentication } from '@/domain/usecases'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockOauth2ClientDetailsModel, mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('AuthenticationService', () => {
  let userRepository: MockProxy<LoadUserByCredentialsRepository>
  let oauth2ClientDetailsRepository: MockProxy<LoadOauth2ClientDetailsRepository>
  let encrypter: MockProxy<Encrypter>
  let sut: Authentication
  let params: Authentication.Input
  let model: LoadUserByCredentialsRepository.Output
  let oauth: LoadOauth2ClientDetailsRepository.Output
  let auth: Encrypter.Output

  beforeAll(() => {
    auth = {
      access_token: faker.datatype.uuid(),
      refresh_token: faker.datatype.uuid(),
      expires_in: faker.datatype.number()
    }
    params = {
      login: faker.internet.email(),
      password: faker.internet.password(),
      clientId: faker.internet.avatar(),
      clientSecret: faker.internet.password(),
      product: faker.datatype.number(),
      grantType: 'password'
    }
    model = { ...mockUserModel() }
    oauth = mockOauth2ClientDetailsModel()
    userRepository = mock()
    oauth2ClientDetailsRepository = mock()
    userRepository.loadByCredentials.mockResolvedValue(model)
    oauth2ClientDetailsRepository.load.mockResolvedValue(oauth)
    encrypter = mock()
    encrypter.encrypt.mockResolvedValue(auth)
  })

  beforeEach(() => {
    sut = new AuthenticationService(userRepository, oauth2ClientDetailsRepository, encrypter)
  })

  it('should call LoadUserByCredentialsRepository with correct email', async () => {
    await sut.auth(params)

    expect(userRepository.loadByCredentials).toHaveBeenCalledWith({ login: params.login })
    expect(userRepository.loadByCredentials).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByCredentialsRepository throws', async () => {
    userRepository.loadByCredentials.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.auth(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call LoadUserByCredentialsRepository with correct email', async () => {
    await sut.auth(params)

    expect(userRepository.loadByCredentials).toHaveBeenCalledWith({ login: params.login })
    expect(userRepository.loadByCredentials).toHaveBeenCalledTimes(1)
  })

  it('should throw if LoadUserByCredentialsRepository throws', async () => {
    userRepository.loadByCredentials.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.auth(params)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call Encrypter with correct value', async () => {
    await sut.auth(params)

    expect(encrypter.encrypt).toHaveBeenCalledWith({ value: model })
    expect(encrypter.encrypt).toHaveBeenCalledTimes(1)
  })

  it('should throw if Encrypter throws', async () => {
    encrypter.encrypt.mockRejectedValueOnce(new Error('encrypt_error'))
    const promise = sut.auth(params)

    await expect(promise).rejects.toThrow(new Error('encrypt_error'))
  })

  it('should return an user on success', async () => {
    const user = await sut.auth(params)

    expect(user).toEqual({ ...auth, scope: oauth.scopes, user: { ...model, password: undefined } })
  })
})

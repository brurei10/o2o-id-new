import { LoadProductServiceByIdRepository, AddUserRepository, AddAccountRepository, AddProfileRepository, Hasher } from '@/domain/protocols'
import { AddUserService } from './add-user-service'
import { AddUser } from '@/domain/usecases'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockAccountModel, mockProductServiceModel, mockUserInput, mockUserModel } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('AddUser Service', () => {
  let productServiceRepository: MockProxy<LoadProductServiceByIdRepository>
  let userRepository: MockProxy<AddUserRepository>
  let accountRepository: MockProxy<AddAccountRepository>
  let profileRepository: MockProxy<AddProfileRepository>
  let hasher: MockProxy<Hasher>
  let hash: string
  let sut: AddUserService

  let input: AddUser.Input
  let productService: LoadProductServiceByIdRepository.Output
  let user: AddUserRepository.Output
  let account: AddAccountRepository.Output

  beforeAll(() => {
    input = mockUserInput()
    productService = mockProductServiceModel()
    account = mockAccountModel()
    user = mockUserModel()
    hash = faker.random.alphaNumeric()
    productServiceRepository = mock()
    accountRepository = mock()
    userRepository = mock()
    profileRepository = mock()
    hasher = mock()

    productServiceRepository.loadById.mockResolvedValue(productService)
    userRepository.add.mockResolvedValue(user)
    accountRepository.add.mockResolvedValue(account)
    hasher.hash.mockResolvedValue(hash)
  })

  beforeEach(() => {
    sut = new AddUserService(productServiceRepository, userRepository, accountRepository, profileRepository, hasher)
  })
  it('should call LoadProductServiceById with correct input', async () => {
    await sut.add(input)

    expect(productServiceRepository.loadById).toHaveBeenCalledWith({ id: input.product.id })
    expect(productServiceRepository.loadById).toHaveBeenCalledTimes(1)
  })

  it('should rethrow if LoadProductServiceById throws', async () => {
    productServiceRepository.loadById.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.add(input)

    await expect(promise).rejects.toThrow(new Error('load_error'))
  })

  it('should call Hasher with correct password', async () => {
    await sut.add(input)

    expect(hasher.hash).toHaveBeenCalledWith({ value: input.password })
    expect(hasher.hash).toHaveBeenCalledTimes(1)
  })

  it('should throw if Hasher throws', async () => {
    hasher.hash.mockRejectedValueOnce(new Error('hash_error'))

    const promise = sut.add(input)
    await expect(promise).rejects.toThrow(new Error('hash_error'))
  })

  it('should call AddUserRepository with correct input', async () => {
    await sut.add(input)

    expect(userRepository.add).toHaveBeenCalledWith({ ...input, password: hash, productServices: [productService] })
    expect(userRepository.add).toHaveBeenCalledTimes(1)
  })

  it('should rethrow if AddUserRepository throws', async () => {
    userRepository.add.mockRejectedValueOnce(new Error('save_error'))
    const promise = sut.add(input)

    await expect(promise).rejects.toThrow(new Error('save_error'))
  })

  it('should call AddAccountRepository with correct input', async () => {
    await sut.add(input)

    expect(accountRepository.add).toHaveBeenCalledWith({ ...input.product.accounts[0], userOwnerId: user.id, productServiceId: productService.id, isActive: true })
    expect(accountRepository.add).toHaveBeenCalledTimes(1)
  })

  it('should rethrow if AddAccountRepository throws', async () => {
    accountRepository.add.mockRejectedValueOnce(new Error('save_error'))
    const promise = sut.add(input)

    await expect(promise).rejects.toThrow(new Error('save_error'))
  })

  it('should call AddProfileRepository with correct input', async () => {
    await sut.add(input)

    const { product } = input

    expect(profileRepository.add).toHaveBeenCalledWith({
      ...product.accounts[0].profiles[0],
      accountId: account.id,
      productServiceId: productService.id,
      features: product.accounts[0].profiles[0].features.map(f => ({
        ...f,
        id: f.id,
        productServiceId: productService.id
      }))
    })
    expect(profileRepository.add).toHaveBeenCalledTimes(1)
  })

  it('should rethrow if AddProfileRepository throws', async () => {
    profileRepository.add.mockRejectedValueOnce(new Error('save_error'))
    const promise = sut.add(input)

    await expect(promise).rejects.toThrow(new Error('save_error'))
  })

  it('should return an user on succes', async () => {
    const result = await sut.add(input)

    expect(result).toEqual({ ...user, password: undefined })
  })
})

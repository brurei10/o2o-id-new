import { AddAccountRepository, AddProfileRepository, AddUserRepository, Hasher, LoadProductServiceByIdRepository } from '@/domain/protocols'
import { AddUser } from '@/domain/usecases'

export class AddUserService implements AddUser {
  constructor (
    private readonly productServiceRepository: LoadProductServiceByIdRepository,
    private readonly userRepository: AddUserRepository,
    private readonly accountRepository: AddAccountRepository,
    private readonly profileRepository: AddProfileRepository,
    private readonly hasher: Hasher
  ) { }

  async add (input: AddUser.Input): Promise<AddUser.Output> {
    const productService = await this.productServiceRepository.loadById({ id: input.product.id })
    const hash = await this.hasher.hash({ value: input.password })
    const user = await this.userRepository.add({ ...input, password: hash, productServices: [productService] })
    for (const acount of input.product.accounts) {
      const account = await this.accountRepository.add({ ...acount, userOwnerId: user.id, productServiceId: productService.id, isActive: true })
      for (const profile of acount.profiles) {
        await this.profileRepository.add({
          ...profile,
          accountId: account.id,
          productServiceId: productService.id,
          features: profile.features.map(f => ({
            ...f,
            id: f.id,
            productServiceId: productService.id
          }))
        })
      }
    }
    return { ...user, password: undefined }
  }
}

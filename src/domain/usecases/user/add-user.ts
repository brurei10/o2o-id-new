import { UserModel } from '@/domain/models'

export interface AddUser {
  add: (input: AddUser.Input) => Promise<AddUser.Output>
}

export namespace AddUser {
  export type Input = {
    cpfCnpj: string
    email: string
    idExternal: string
    login: string
    name: string
    password: string
    phone: string
    userLanguage: string
    whatsPhone: string
    product: {
      id: number
      accounts: Account[]
    }
  }
  export type Account = {
    contractCode: string
    empresaContrato: string
    productName: string
    productServiceId: number
    profiles: Array<{
      name?: string
      features: Array<{
        id: number
        name?: string
      }>
    }>
  }
  export type Output = UserModel
}

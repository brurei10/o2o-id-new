
export interface UpdatePassword {
  reset: (input: UpdatePassword.Input) => Promise<void>
}

export namespace UpdatePassword {
  export type Input = {
    email: string
    password: string
    resetToken: string
  }
}

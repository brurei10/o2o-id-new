
export interface ResetPassword {
  send: (input: ResetPassword.Input) => Promise<void>
}

export namespace ResetPassword {
  export type Input = {
    email: string
  }
}

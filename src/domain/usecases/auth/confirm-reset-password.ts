export interface ConfirmResetPassword {
  confirm: (input: ConfirmResetPassword.Input) => Promise<ConfirmResetPassword.Output>
}

export namespace ConfirmResetPassword {
  export type Input = {
    email: string
    code: string
  }
  export type Output = {
    resetToken: string
    email: string
  }
}

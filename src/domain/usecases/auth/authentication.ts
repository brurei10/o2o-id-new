import { UserModel } from '@/domain/models'

export interface Authentication {
  auth: (input: Authentication.Input) => Promise<Authentication.Output>
}

export namespace Authentication {
  export type Input = {
    login: string
    password: string
    clientId: string
    clientSecret: string
    product: number
    grantType: string
  }

  export type Output = {
    refresh_token: string
    access_token: string
    expires_in: number
    scope: string
    user: UserModel
  }
}

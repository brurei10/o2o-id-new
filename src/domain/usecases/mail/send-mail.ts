export interface SendMail {
  send: (input: SendMail.Input) => Promise<SendMail.Output>
}

export namespace SendMail {
  export type Input = {
    from: string
    to: string
    subject: string
    viewPath: string
    data: any
  }
  export type Output = any
}

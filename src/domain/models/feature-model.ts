import { EntityModel } from './entity-model'
import { ProductServiceModel } from './product-service-model'
import { ProfileModel } from './profile-model'

export type FeatureModel = EntityModel & {
  productServiceId: number
  name?: string
  productService?: ProductServiceModel
  profiles?: ProfileModel[]
}


export type EntityModel = {
  id?: number
  createdAt?: Date
  updatedAt?: Date
}

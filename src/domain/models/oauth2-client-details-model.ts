export type Oauth2ClientDetailsModel = {
  id?: string
  accessTokenValidity: number
  authorities: string
  clientId: string
  clientSecret: string
  grantTypes: string
  redirectUris: string
  refreshTokenValidity: number
  resources: string
  scopes: string
  secretRequired: boolean
  createdAt?: Date
  updatedAt?: Date
}

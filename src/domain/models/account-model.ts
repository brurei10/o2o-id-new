import { AccountUserModel } from './account-user-model'
import { EntityModel } from './entity-model'
import { ProductServiceModel } from './product-service-model'
import { ProfileModel } from './profile-model'
import { UserAccountProfileModel } from './user-account-profile-model'
import { UserModel } from './user-model'

export type AccountModel = EntityModel & {
  userOwnerId: number
  productServiceId: number
  contractCode: string
  cpfCnpjOWner?: string
  empresaContrato: string
  filialContrato?: string
  productName?: string
  isActive: boolean
  profiles?: ProfileModel[]
  accountUsers?: AccountUserModel[]
  userAccountProfiles?: UserAccountProfileModel[]
  productService?: ProductServiceModel
  userOwner?: UserModel
}

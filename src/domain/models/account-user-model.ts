import { AccountModel } from './account-model'
import { EntityModel } from './entity-model'
import { UserModel } from './user-model'

export type AccountUserModel = EntityModel & {
  accountId: number
  userId: number
  isAdmin: boolean
  account?: AccountModel
  user?: UserModel
}

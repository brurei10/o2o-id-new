import { AccountModel } from './account-model'
import { EntityModel } from './entity-model'
import { FeatureModel } from './feature-model'
import { ProductServiceModel } from './product-service-model'
import { UserAccountProfileModel } from './user-account-profile-model'

export type ProfileModel = EntityModel & {
  accountId: number
  productServiceId: number
  name?: string
  account?: AccountModel
  productService?: ProductServiceModel
  userAccountProfiles?: UserAccountProfileModel[]
  features?: FeatureModel[]
}

import { UserModel } from './user-model'

export type AccessCodeModel = {
  id?: number
  userId: number
  code: string
  validAt: Date
  type: 'reset_password'
  createdAt?: Date
  updatedAt?: Date
  user?: UserModel
}

import { AccountModel } from './account-model'
import { EntityModel } from './entity-model'
import { FeatureModel } from './feature-model'
import { ProfileModel } from './profile-model'
import { UserModel } from './user-model'

export type ProductServiceModel = EntityModel & {
  ownerProductId: number
  description: string
  name: string
  accounts?: AccountModel[]
  profiles?: ProfileModel[]
  productServices?: ProductServiceModel[]
  features?: FeatureModel[]
  productService?: ProductServiceModel
  users?: UserModel[]
}

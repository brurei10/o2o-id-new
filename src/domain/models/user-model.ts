import { AccountModel } from './account-model'
import { AccountUserModel } from './account-user-model'
import { EntityModel } from './entity-model'
import { ProductServiceModel } from './product-service-model'
import { UserAccountProfileModel } from './user-account-profile-model'

export type UserModel = EntityModel & {
  name: string
  email: string
  phone: string
  password: string
  cpfCnpj: string
  idExternal: string
  login: string
  whatsPhone: string
  userLanguage: string
  token?: string
  firstAccess?: boolean
  active?: boolean
  lastAccess?: Date
  accounts?: AccountModel[]
  accountUsers?: AccountUserModel[]
  userAccountProfiles?: UserAccountProfileModel[]
  productServices?: ProductServiceModel[]
}

import { AccountModel } from './account-model'
import { EntityModel } from './entity-model'
import { ProfileModel } from './profile-model'
import { UserModel } from './user-model'

export type UserAccountProfileModel = EntityModel & {
  accountId: number
  profileId: number
  userId: number
  user?: UserModel
  profile?: ProfileModel
  account?: AccountModel
}

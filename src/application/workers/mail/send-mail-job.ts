import { SendMail } from '@/domain/usecases'
import { Worker } from '@/application/protocols'

type Input = SendMail.Input

export class SendMailWorker implements Worker {
  constructor (
    private readonly sendMail: SendMail
  ) {}

  async handle (input: Input): Promise<void> {
    await this.sendMail.send(input)
  }
}

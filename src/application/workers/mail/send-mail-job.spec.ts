import { SendMailWorker } from '@/application/workers'
import { SendMail } from '@/domain/usecases'

import { mock, MockProxy } from 'jest-mock-extended'
import { faker } from '@faker-js/faker'

describe('SendMailWorker', () => {
  let sut: SendMailWorker
  let sendMail: MockProxy<SendMail>
  let input: SendMail.Input

  beforeAll(() => {
    input = {
      from: faker.internet.email(),
      to: faker.internet.email(),
      subject: faker.random.words(),
      viewPath: faker.internet.url(),
      data: faker.datatype.json()
    }

    sendMail = mock()
  })

  beforeEach(() => {
    sut = new SendMailWorker(sendMail)
  })

  it('should call SendMail with correct input', async () => {
    await sut.handle(input)

    expect(sendMail.send).toHaveBeenCalledWith(input)
    expect(sendMail.send).toHaveBeenCalledTimes(1)
  })

  it('should throw if SendMail throws', async () => {
    sendMail.send.mockRejectedValueOnce(new Error('infra_error'))

    const promise = sut.handle(input)
    expect(promise).rejects.toThrow(new Error('infra_error'))
  })
})

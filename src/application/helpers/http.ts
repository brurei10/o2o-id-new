import { ForbiddenError, ServerError, UnauthorizedError, HttpError } from '@/application/errors'

export type HttpResponse<T = any> = {
  statusCode: number
  data: T
}

export const ok = <T = any> (data?: T): HttpResponse<T> => ({
  statusCode: 200,
  data
})

export const customErrorResponse = (error: HttpError): HttpResponse<Error> => ({
  statusCode: error.code,
  data: error
})

export const badRequest = (error: Error): HttpResponse<Error> => ({
  statusCode: 400,
  data: error
})

export const unauthorized = (): HttpResponse<Error> => ({
  statusCode: 401,
  data: new UnauthorizedError()
})

export const forbidden = (error?: Error): HttpResponse<Error> => ({
  statusCode: 403,
  data: error ?? new ForbiddenError()
})

export const serverError = (error: unknown): HttpResponse<Error> => ({
  statusCode: 500,
  data: new ServerError(error instanceof Error ? error : undefined)
})

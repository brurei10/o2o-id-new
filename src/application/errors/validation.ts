export class EmailInUseError extends Error {
  constructor () {
    super('The email already in use')
    this.name = 'EmailInUseError'
  }
}

export class RequiredFieldError extends Error {
  constructor (fieldName?: string) {
    const message = fieldName === undefined
      ? 'Campo requerido'
      : `O campo ${fieldName} é obrigatório.`
    super(message)
    this.name = 'RequiredFieldError'
  }
}

export class InvalidParamError extends Error {
  constructor (message?: string) {
    super(message)
    this.name = 'InvalidParamError'
  }
}

export class InvalidMimeTypeError extends Error {
  constructor (allowed: string[]) {
    super(`Unsupported file. Allowed extensions: ${allowed.join(', ')}`)
    this.name = 'InvalidMimeTypeError'
  }
}

export class MaxFileSizeError extends Error {
  constructor (maxSizeInMb: number) {
    super(`File upload limit is ${maxSizeInMb}MB`)
    this.name = 'MaxFileSizeError'
  }
}

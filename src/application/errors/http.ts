export class ServerError extends Error {
  constructor (error?: Error) {
    super('Server failed. Try again soon')
    this.name = 'ServerError'
    this.stack = error?.stack
  }
}

export class UnauthorizedError extends Error {
  constructor () {
    super('Unauthorized')
    this.name = 'UnauthorizedError'
  }
}

export class ForbiddenError extends Error {
  constructor () {
    super('Access denied')
    this.name = 'ForbiddenError'
  }
}

export class NoResultError extends Error {
  constructor (entity?: string) {
    super(`No result of ${entity} was found.`)
    this.name = 'NoResultError'
  }
}

export class CustomResultMessageError extends Error {
  constructor (message?: string) {
    super(message)
    this.name = 'message'
  }
}

export class PaymentError extends Error {
  constructor () {
    super('Payment fails.')
    this.name = 'PaymentError'
  }
}

export class OperationFailureError extends Error {
  constructor () {
    super('Operation failure.')
    this.name = 'OperationFailure'
  }
}

export class SubscriptionError extends Error {
  constructor () {
    super('Fakha na assinatura. Tente novamente.')
    this.name = 'SubscriptionError'
  }
}

import { UpdatePassword } from '@/domain/usecases'
import { Validation } from '@/domain/validation'
import { MainController } from '@/application/controllers'
import { HttpResponse, ok } from '@/application/helpers'
import { ValidationBuilder as Builder, Validator } from '@/application/validation'

type HttpRequest = UpdatePassword.Input

export class UpdatePasswordController extends MainController {
  constructor (
    private readonly validation: Validation<UpdatePassword.Input>,
    private readonly UpdatePassword: UpdatePassword
  ) {
    super()
  }

  async perform ({ email, password, resetToken }: HttpRequest): Promise<HttpResponse<any>> {
    await this.validation.validate({ email, password, resetToken })
    await this.UpdatePassword.reset({ email, password, resetToken })
    return ok()
  }

  override buildValidators ({ email, password, resetToken }: HttpRequest): Validator[] {
    return [
      ...Builder.of({ value: email, fieldName: 'email' }).required().email().build(),
      ...Builder.of({ value: password, fieldName: 'senha' }).required().build(),
      ...Builder.of({ value: resetToken, fieldName: 'token de verificação' }).required().build()
    ]
  }
}

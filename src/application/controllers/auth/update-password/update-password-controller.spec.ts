import { MainController, UpdatePasswordController } from '@/application/controllers'
import { UpdatePassword } from '@/domain/usecases'

import { faker } from '@faker-js/faker'
import { mock, MockProxy } from 'jest-mock-extended'
import { Validation } from '@/domain/validation'

describe('UpdatePasswordController', () => {
  let sut: UpdatePasswordController
  let updatePassword: MockProxy<UpdatePassword>
  let validation: MockProxy<Validation<UpdatePassword.Input>>
  let params: UpdatePassword.Input

  beforeAll(() => {
    params = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      resetToken: faker.datatype.uuid()
    }
    validation = mock()
    validation.validate.mockResolvedValue()
    updatePassword = mock()
  })

  beforeEach(() => {
    sut = new UpdatePasswordController(validation, updatePassword)
  })

  it('should extends Controller', () => {
    expect(sut).toBeInstanceOf(MainController)
  })

  it('should call Validation with correct input', async () => {
    await sut.handle(params)

    expect(validation.validate).toHaveBeenCalledWith(params)
    expect(validation.validate).toHaveBeenCalledTimes(1)
  })

  it('should call UpdatePassword with correct input', async () => {
    await sut.handle(params)

    expect(updatePassword.reset).toHaveBeenCalledWith(params)
    expect(updatePassword.reset).toHaveBeenCalledTimes(1)
  })

  it('should return 200 if add succeeds', async () => {
    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 200
    })
  })
})

import { MainController, ResetPasswordController } from '@/application/controllers'
import { ResetPassword } from '@/domain/usecases'

import { faker } from '@faker-js/faker'
import { mock, MockProxy } from 'jest-mock-extended'
import { ServerError } from '@/application/errors'
import { Validation } from '@/domain/validation'

describe('ResetPasswordController', () => {
  let sut: ResetPasswordController
  let validation: MockProxy<Validation<ResetPassword.Input>>
  let sendMailConfirmAccess: MockProxy<ResetPassword>
  let params: ResetPassword.Input

  beforeAll(() => {
    params = {
      email: faker.internet.email()
    }

    sendMailConfirmAccess = mock()
    validation = mock()
  })

  beforeEach(() => {
    sut = new ResetPasswordController(validation,sendMailConfirmAccess)
  })

  it('should extends Controller', () => {
    expect(sut).toBeInstanceOf(MainController)
  })

  it('should call Validation with correct input', async () => {
    await sut.handle(params)

    expect(validation.validate).toHaveBeenCalledWith({ email: params.email })
    expect(validation.validate).toHaveBeenCalledTimes(1)
  })

  it('should call ResetPassword with correct input', async () => {
    await sut.handle(params)

    expect(sendMailConfirmAccess.send).toHaveBeenCalledWith({ email: params.email })
    expect(sendMailConfirmAccess.send).toHaveBeenCalledTimes(1)
  })

  it('should return 500 on infra error', async () => {
    const error = new Error('infra_error')
    sendMailConfirmAccess.send.mockRejectedValueOnce(error)

    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 500,
      data: new ServerError(error)
    })
  })

  it('should return 200 if add succeeds', async () => {
    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 200
    })
  })
})

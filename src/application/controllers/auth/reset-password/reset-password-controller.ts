import { ResetPassword } from '@/domain/usecases'
import { MainController } from '@/application/controllers'
import { HttpResponse, ok } from '@/application/helpers'
import { ValidationBuilder as Builder, Validator } from '@/application/validation'
import { Validation } from '@/domain/validation'

type HttpRequest = ResetPassword.Input

export class ResetPasswordController extends MainController {
  constructor (
    private readonly validation: Validation<ResetPassword.Input>,
    private readonly resetPassword: ResetPassword
  ) {
    super()
  }

  async perform ({ email }: HttpRequest): Promise<HttpResponse<any>> {
    await this.validation.validate({ email })
    await this.resetPassword.send({ email })
    return ok()
  }

  override buildValidators ({ email }: HttpRequest): Validator[] {
    return [
      ...Builder.of({ value: email, fieldName: 'email' }).required().email().build()
    ]
  }
}

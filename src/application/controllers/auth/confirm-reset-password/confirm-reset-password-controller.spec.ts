import { MainController, ConfirmResetPasswordController } from '@/application/controllers'
import { ConfirmResetPassword } from '@/domain/usecases'
import { faker } from '@faker-js/faker'
import { mock, MockProxy } from 'jest-mock-extended'
import { Validation } from '@/domain/validation'

describe('ConfirmResetPasswordController', () => {
  let sut: ConfirmResetPasswordController
  let validation: MockProxy<Validation<ConfirmResetPassword.Input>>
  let confirmAccess: MockProxy<ConfirmResetPassword>
  let params: ConfirmResetPassword.Input

  const model = { resetToken: faker.datatype.uuid(), email: faker.internet.email() }

  beforeAll(() => {
    params = {
      email: faker.internet.email(),
      code: faker.random.alphaNumeric()
    }
    validation = mock()
    validation.validate.mockResolvedValue()
    confirmAccess = mock()
    confirmAccess.confirm.mockResolvedValue(model)
  })

  beforeEach(() => {
    sut = new ConfirmResetPasswordController(validation, confirmAccess)
  })

  it('should extends Controller', () => {
    expect(sut).toBeInstanceOf(MainController)
  })

  it('should call DbResetPasswordValidation with correct input', async () => {
    await sut.perform(params)

    expect(validation.validate).toHaveBeenCalledWith({ email: params.email, code: params.code })
    expect(validation.validate).toHaveBeenCalledTimes(1)
  })

  it('should call ConfirmResetPassword with correct input', async () => {
    await sut.perform(params)

    expect(confirmAccess.confirm).toHaveBeenCalledWith(params)
    expect(confirmAccess.confirm).toHaveBeenCalledTimes(1)
  })

  it('should return 200 if add succeeds', async () => {
    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 200,
      data: model
    })
  })
})

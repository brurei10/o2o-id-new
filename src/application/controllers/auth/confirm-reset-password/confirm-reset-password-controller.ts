import { ConfirmResetPassword } from '@/domain/usecases'
import { MainController } from '@/application/controllers'
import { HttpResponse, ok } from '@/application/helpers'
import { ValidationBuilder as Builder, Validator } from '@/application/validation'
import { Validation } from '@/domain/validation'

type HttpRequest = ConfirmResetPassword.Input

export class ConfirmResetPasswordController extends MainController {
  constructor (
    private readonly validation: Validation<ConfirmResetPassword.Input>,
    private readonly confirmAccess: ConfirmResetPassword
  ) {
    super()
  }

  async perform ({ email, code }: HttpRequest): Promise<HttpResponse<any>> {
    await this.validation.validate({ email, code })
    const response = await this.confirmAccess.confirm({ email, code })
    return ok(response)
  }

  override buildValidators ({ email, code }: HttpRequest): Validator[] {
    return [
      ...Builder.of({ value: code, fieldName: 'código' }).required().build(),
      ...Builder.of({ value: email, fieldName: 'email' }).required().email().build()
    ]
  }
}

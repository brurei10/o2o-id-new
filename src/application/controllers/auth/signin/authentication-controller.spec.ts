import { MainController, AuthenticationController } from '@/application/controllers'
import { Authentication } from '@/domain/usecases'

import { faker } from '@faker-js/faker'
import { mock, MockProxy } from 'jest-mock-extended'
import { mockUserModel } from '@/domain/test'
import { ServerError, UnauthorizedError } from '@/application/errors'
import { Validation } from '@/domain/validation'

describe('AuthenticationController', () => {
  let sut: AuthenticationController
  let authentication: MockProxy<Authentication>
  let validation: MockProxy<Validation<Authentication.Input>>
  let auth: Authentication.Output
  let params: {
    login: string
    password: string
    grantType: string
    authorization: string
    product: number
  }

  beforeAll(() => {
    auth = {
      user: mockUserModel(),
      access_token: faker.datatype.uuid(),
      refresh_token: faker.datatype.uuid(),
      expires_in: faker.datatype.number(),
      scope: faker.datatype.string()
    }
    params = {
      login: faker.internet.email(),
      password: faker.random.word(),
      authorization: 'Basic cmVub3ZpOjEyMzQ1Ng==',
      grantType: 'password',
      product: faker.datatype.number()
    }

    authentication = mock()
    validation = mock()
    authentication.auth.mockResolvedValue(auth)
  })

  beforeEach(() => {
    sut = new AuthenticationController(validation, authentication)
  })

  it('should extends Controller', () => {
    expect(sut).toBeInstanceOf(MainController)
  })

  it('should call Validation with correct input', async () => {
    await sut.handle(params)

    expect(validation.validate).toHaveBeenCalledWith({
      ...params,
      clientId: 'o2o',
      clientSecret: '123456'
    })
    expect(validation.validate).toHaveBeenCalledTimes(1)
  })

  it('should call AddUser with correct input', async () => {
    await sut.handle(params)

    expect(authentication.auth).toHaveBeenCalledWith({
      ...params,
      clientId: 'o2o',
      clientSecret: '123456'
    })
    expect(authentication.auth).toHaveBeenCalledTimes(1)
  })

  it('should return 500 on infra error', async () => {
    const error = new Error('infra_error')
    authentication.auth.mockRejectedValueOnce(error)

    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 500,
      data: new ServerError(error)
    })
  })

  it('should return 401 on infra error', async () => {
    authentication.auth.mockResolvedValueOnce(null)

    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 401,
      data: new UnauthorizedError()
    })
  })

  it('should return 200 if add succeeds', async () => {
    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 200,
      data: auth
    })
  })
})

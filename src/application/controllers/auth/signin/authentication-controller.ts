import { Authentication } from '@/domain/usecases'
import { Validation } from '@/domain/validation'
import { MainController } from '@/application/controllers'
import { HttpResponse, ok, unauthorized } from '@/application/helpers'
import { ValidationBuilder as Builder, Validator } from '@/application/validation'

type HttpRequest = {
  login: string
  password: string
  grantType: string
  authorization: string
  product: number
}

export class AuthenticationController extends MainController {
  constructor (
    private readonly validation: Validation<Authentication.Input>,
    private readonly authentication: Authentication
  ) {
    super()
  }

  async perform (request: HttpRequest): Promise<HttpResponse<any>> {
    const b64auth = (request.authorization || '').split(' ')[1] || ''
    const [clientId, clientSecret] = Buffer.from(b64auth, 'base64').toString().split(':')
    await this.validation.validate({ ...request, clientId, clientSecret })
    const auth = await this.authentication.auth({ ...request, clientId, clientSecret })
    if (!auth) {
      return unauthorized()
    }
    return ok(auth)
  }

  override buildValidators ({ password, login }: HttpRequest): Validator[] {
    return [
      ...Builder.of({ value: login, fieldName: 'login' }).required().build(),
      ...Builder.of({ value: password, fieldName: 'senha' }).required().build()
    ]
  }
}

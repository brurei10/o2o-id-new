import { MainController } from '@/application/controllers'
import { HttpResponse, ok } from '@/application/helpers'
import { ValidationBuilder as Builder, Validator } from '@/application/validation'
import { AddUser } from '@/domain/usecases'
import { Validation } from '@/domain/validation'

type HttpRequest = AddUser.Input

export class AddUserController extends MainController {
  constructor (
    private readonly validation: Validation<AddUser.Input>,
    private readonly addUser: AddUser
  ) {
    super()
  }

  async perform (input: HttpRequest): Promise<HttpResponse<any>> {
    await this.validation.validate(input)
    const user = await this.addUser.add(input)
    return ok(user)
  }

  override buildValidators ({ cpfCnpj, email, idExternal, login, name, password, phone }: HttpRequest): Validator[] {
    return [
      ...Builder.of({ value: cpfCnpj, fieldName: 'cpf/cnpj' }).required().build(),
      ...Builder.of({ value: email, fieldName: 'email' }).required().email().build(),
      ...Builder.of({ value: idExternal, fieldName: 'id externo' }).required().build(),
      ...Builder.of({ value: phone, fieldName: 'telefone' }).required().phone().build(),
      ...Builder.of({ value: name, fieldName: 'nome' }).required().build(),
      ...Builder.of({ value: login, fieldName: 'login' }).required().build(),
      ...Builder.of({ value: password, fieldName: 'password' }).required().build()
    ]
  }
}

import { MainController, AddUserController } from '@/application/controllers'
import { AddUser } from '@/domain/usecases'
import { ServerError } from '@/application/errors'

import { mock, MockProxy } from 'jest-mock-extended'
import { mockUserModel, mockUserInput } from '@/domain/test'
import { Validation } from '@/domain/validation'

describe('AddUserController', () => {
  let sut: AddUserController
  let addUser: MockProxy<AddUser>
  let validation: MockProxy<Validation<AddUser.Input>>
  let model: AddUser.Output
  let params: AddUser.Input

  beforeAll(() => {
    model = mockUserModel()
    params = mockUserInput()

    addUser = mock()
    validation = mock()
    addUser.add.mockResolvedValue(model)
  })

  beforeEach(() => {
    sut = new AddUserController(validation, addUser)
  })

  it('should extends Controller', () => {
    expect(sut).toBeInstanceOf(MainController)
  })

  it('should call Validation with correct input', async () => {
    await sut.handle(params)

    expect(validation.validate).toHaveBeenCalledWith(params)
    expect(validation.validate).toHaveBeenCalledTimes(1)
  })

  it('should call AddUser with correct input', async () => {
    await sut.handle(params)

    expect(addUser.add).toHaveBeenCalledWith(params)
    expect(addUser.add).toHaveBeenCalledTimes(1)
  })

  it('should return 500 on infra error on Validation', async () => {
    const error = new Error('infra_error')
    validation.validate.mockRejectedValueOnce(error)

    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 500,
      data: new ServerError(error)
    })
  })

  it('should return 500 on infra error on AddUser', async () => {
    const error = new Error('infra_error')
    addUser.add.mockRejectedValueOnce(error)

    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 500,
      data: new ServerError(error)
    })
  })

  it('should return 200 if add succeeds', async () => {
    const httpResponse = await sut.handle(params)

    expect(httpResponse).toEqual({
      statusCode: 200,
      data: model
    })
  })
})

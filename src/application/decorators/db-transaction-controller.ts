import { DbTransaction } from '@/application/contracts'
import { MainController } from '@/application/controllers'
import { HttpResponse } from '@/application/helpers'
import { SaveLogRepository } from '@/domain/protocols'
import { Controller } from '../protocols'

export class DbTransactionController extends MainController {
  constructor (
    private readonly decoratee: Controller,
    private readonly db: DbTransaction,
    private readonly logRepository: SaveLogRepository
  ) {
    super()
  }

  async perform (httpRequest: any): Promise<HttpResponse> {
    await this.db.openTransaction()
    try {
      const httpResponse = await this.decoratee.handle(httpRequest)
      await this.db.commit()
      return httpResponse
    } catch (error) {
      await this.db.rollback()
      await this.logRepository.save({ content: error.stack, type: 'request_fail' })
      throw error
    } finally {
      await this.db.closeTransaction()
    }
  }
}

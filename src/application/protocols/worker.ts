export interface Worker {
  handle: (input: any) => Promise<void>
}

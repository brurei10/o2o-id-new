import { Required, RequiredBuffer, RequiredString, ValidationBuilder, Email, Cpf, Phone, ComparePassword } from '@/application/validation'
import { faker } from '@faker-js/faker'

describe('ValidationBuilder', () => {
  it('should return RequiredString', () => {
    const value = faker.database.column()
    const validators = ValidationBuilder
      .of({ value })
      .required()
      .build()

    expect(validators).toEqual([new RequiredString(value)])
  })

  it('should return EmailValidation', () => {
    const email = faker.internet.email()
    const validators = ValidationBuilder
      .of({ value: email })
      .email()
      .build()

    expect(validators).toEqual([new Email(email)])
  })

  it('should return PhoneValidation', () => {
    const phone = faker.phone.number()
    const validators = ValidationBuilder
      .of({ value: phone })
      .phone()
      .build()

    expect(validators).toEqual([new Phone(phone)])
  })

  it('should return ComparePassword', () => {
    const pass = faker.internet.password()
    const password = faker.internet.password()
    const validators = ValidationBuilder
      .of({ value: pass })
      .compare(password)
      .build()

    expect(validators).toEqual([new ComparePassword(pass, password)])
  })

  it('should return CpfValidation', () => {
    const value = faker.database.column()
    const validators = ValidationBuilder
      .of({ value })
      .cpf()
      .build()

    expect(validators).toEqual([new Cpf(value)])
  })

  it('should return RequiredBuffer', () => {
    const buffer = Buffer.from(faker.datatype.string())

    const validators = ValidationBuilder
      .of({ value: buffer })
      .required()
      .build()

    expect(validators).toEqual([new RequiredBuffer(buffer)])
  })

  it('should return Required', () => {
    const value = faker.database.column()
    const validators = ValidationBuilder
      .of({ value: { value } })
      .required()
      .build()

    expect(validators).toEqual([new Required({ value })])
  })

  it('should return Required', () => {
    const buffer = Buffer.from(faker.datatype.string())

    const validators = ValidationBuilder
      .of({ value: { buffer } })
      .required()
      .build()

    expect(validators).toEqual([
      new Required({ buffer }),
      new RequiredBuffer(buffer)
    ])
  })
})

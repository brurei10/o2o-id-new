import {
  Validator,
  Required,
  RequiredBuffer,
  RequiredString,
  Email,
  ComparePassword,
  Cpf,
  Phone
} from '@/application/validation'

export class ValidationBuilder {
  protected constructor (
    protected readonly value: any,
    protected readonly fieldName?: string,
    protected readonly validators: Validator[] = []
  ) {}

  static of ({ value, fieldName }: { value: any, fieldName?: string }): ValidationBuilder {
    return new ValidationBuilder(value, fieldName)
  }

  required (): ValidationBuilder {
    if (this.value instanceof Buffer) {
      this.validators.push(new RequiredBuffer(this.value, this.fieldName))
    } else if (typeof this.value === 'string') {
      this.validators.push(new RequiredString(this.value, this.fieldName))
    } else {
      this.validators.push(new Required(this.value, this.fieldName))
      if (this.value?.buffer !== undefined) {
        this.validators.push(new RequiredBuffer(this.value.buffer, this.fieldName))
      }
    }
    return this
  }

  email (): ValidationBuilder {
    this.validators.push(new Email(this.value))
    return this
  }

  cpf (): ValidationBuilder {
    this.validators.push(new Cpf(this.value))
    return this
  }

  phone (): ValidationBuilder {
    this.validators.push(new Phone(this.value))
    return this
  }

  compare (value: string): ValidationBuilder {
    this.validators.push(new ComparePassword(this.value, value))
    return this
  }

  build (): Validator[] {
    return this.validators
  }
}

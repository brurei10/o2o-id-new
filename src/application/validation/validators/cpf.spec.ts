import { InvalidParamError } from '@/application/errors'
import { Cpf } from './cpf'

import { faker } from '@faker-js/faker'

describe('Cpf Validator', () => {
  let value: string

  beforeAll(() => {
    value = faker.random.word()
  })
  it('should return a InvalidParamError if validation fails', () => {
    const sut = new Cpf(value)
    const error = sut.validate()
    expect(error).toEqual(new InvalidParamError('Invalid cpf.'))
  })

  it('should return if validation succeds', () => {
    const sut = new Cpf('520.758.090-62')
    const error = sut.validate()
    expect(error).toBeFalsy()
  })
})

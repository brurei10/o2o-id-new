import { InvalidParamError } from '@/application/errors'
import { Phone } from './phone'

import { faker } from '@faker-js/faker'

describe('Phone Validator', () => {
  let value: string

  beforeAll(() => {
    value = faker.phone.number()
  })
  it('should return a InvalidParamError if validation fails', () => {
    const sut = new Phone(value)
    const error = sut.validate()
    expect(error).toEqual(new InvalidParamError('Invalid phone.'))
  })

  it('should return if validation succeds', () => {
    const sut = new Phone('31998765432')
    const error = sut.validate()
    expect(error).toBeFalsy()
  })
})

import { InvalidParamError } from '@/application/errors'
import { Email } from './email'

import { faker } from '@faker-js/faker'

describe('Email Validator', () => {
  let value: string

  beforeAll(() => {
    value = faker.random.word()
  })
  it('should return a InvalidParamError if validation fails', () => {
    const sut = new Email(value)
    const error = sut.validate()
    expect(error).toEqual(new InvalidParamError('Invalid email.'))
  })

  it('should return if validation succeds', () => {
    const sut = new Email(faker.internet.email())
    const error = sut.validate()
    expect(error).toBeFalsy()
  })
})

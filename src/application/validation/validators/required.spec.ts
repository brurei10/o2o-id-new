import { RequiredFieldError } from '@/application/errors'
import { Required, RequiredString, RequiredBuffer } from '@/application/validation'

import { faker } from '@faker-js/faker'

describe('Required Validator', () => {
  let field: string
  let value: string
  let buffer: string

  beforeAll(() => {
    field = faker.database.column()
    value = faker.random.word()
    buffer = faker.random.alphaNumeric()
  })

  describe('Required', () => {
    it('should return RequiredFieldError if value is null', () => {
      const sut = new Required(null as any, field)
      const error = sut.validate()
      expect(error).toEqual(new RequiredFieldError(field))
    })

    it('should return RequiredFieldError if value is undefined', () => {
      const sut = new Required(undefined as any, field)
      const error = sut.validate()
      expect(error).toEqual(new RequiredFieldError(field))
    })

    it('should return undefined if value is not empty', () => {
      const sut = new Required(value, field)
      const error = sut.validate()
      expect(error).toBeUndefined()
    })
  })

  describe('RequiredString', () => {
    it('should extend Required', () => {
      const sut = new RequiredString('')
      expect(sut).toBeInstanceOf(Required)
    })

    it('should return RequiredFieldError if value is empty', () => {
      const sut = new RequiredString('', field)
      const error = sut.validate()
      expect(error).toEqual(new RequiredFieldError(field))
    })

    it('should return undefined if value is not empty', () => {
      const sut = new RequiredString(value, field)
      const error = sut.validate()
      expect(error).toBeUndefined()
    })
  })

  describe('RequiredBuffer', () => {
    it('should extend Required', () => {
      const sut = new RequiredString('')
      expect(sut).toBeInstanceOf(Required)
    })

    it('should return RequiredFieldError if value is empty', () => {
      const sut = new RequiredBuffer(Buffer.from(''))
      const error = sut.validate()
      expect(error).toEqual(new RequiredFieldError())
    })

    it('should return undefined if value is not empty', () => {
      const sut = new RequiredBuffer(Buffer.from(buffer))
      const error = sut.validate()
      expect(error).toBeUndefined()
    })
  })
})

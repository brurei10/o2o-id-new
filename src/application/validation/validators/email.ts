import { Validator } from '@/application/validation'
import { InvalidParamError } from '@/application/errors'

export class Email implements Validator {
  constructor (readonly value: string) {}

  validate (): Error {
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return (!this.value || emailRegex.test(this.value.toLowerCase())) ? undefined : new InvalidParamError('Invalid email.')
  }
}

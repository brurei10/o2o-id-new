import { Validator } from '@/application/validation'
import { InvalidParamError } from '@/application/errors'

export class ComparePassword implements Validator {
  constructor (readonly value: any, readonly valueToCompare: any) {}
  validate (): Error {
    if (this.value !== this.valueToCompare) {
      return new InvalidParamError('Password does not match.')
    }
  }
}

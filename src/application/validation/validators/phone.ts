import { Validator } from '@/application/validation'
import { InvalidParamError } from '@/application/errors'

export class Phone implements Validator {
  constructor (readonly value: string) {}

  validate (): Error {
    const phoneRegex = /^(?:(?:\+|00)?(55)\s?)?(?:(?:\(?[1-9][0-9]\)?)?\s?)?(?:((?:9\d|[2-9])\d{3})-?(\d{4}))$/
    return (!this.value || phoneRegex.test(this.value.toLowerCase())) ? undefined : new InvalidParamError('Invalid phone.')
  }
}

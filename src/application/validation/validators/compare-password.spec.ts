import { InvalidParamError } from '@/application/errors'
import { ComparePassword } from './compare-password'

import { faker } from '@faker-js/faker'

describe('ComparePassword Validator', () => {
  let value: string
  let valueToCompare: string

  beforeAll(() => {
    value = faker.random.word()
    valueToCompare = faker.random.word()
  })
  it('should return a InvalidParamError if validation fails', () => {
    const sut = new ComparePassword(value, valueToCompare)
    const error = sut.validate()
    expect(error).toEqual(new InvalidParamError('Password does not match.'))
  })

  it('should return if validation succeds', () => {
    const sut = new ComparePassword(value, value)
    const error = sut.validate()
    expect(error).toBeFalsy()
  })
})

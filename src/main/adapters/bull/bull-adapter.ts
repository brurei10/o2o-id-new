import { QueueAdapter, SaveLogRepository } from '@/domain/protocols'
import { Worker } from '@/application/protocols'
import Queue from 'bull'

export class BullAdapter implements QueueAdapter {
  constructor (
    private readonly name: string,
    private readonly worker: Worker,
    private readonly saveLogRepository: SaveLogRepository
  ) {}

  async add (input: any): Promise<void> {
    if (process.env.NODE_ENV !== 'test') {
      const proccesQueue = new Queue(this.name, process.env.REDIS_URL)
      proccesQueue.process(async (job, done) => {
        try {
          await this.worker.handle(job.data)
        } catch (error) {
          await this.saveLogRepository.save({ content: error.stack, type: 'process_queue' })
        } finally {
          done()
        }
      })
      proccesQueue.add(input, {
        attempts: 5,
        removeOnComplete: true,
        removeOnFail: true
      })
    }
  }
}

import * as dotenv from 'dotenv'
dotenv.config()

export const env = {
  port: process.env.PORT ?? 5050,
  accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
  refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET,
  refeshTokenExpiry: parseInt(process.env.REFRESH_TOKEN_EXPIRY) ,
  accessTokenExpiry: parseInt(process.env.ACCESS_TOKEN_EXPIRY),
  aws: {
    mailHost: process.env.MAIL_HOST,
    mailPort: process.env.MAIL_PORT,
    mailUser: process.env.MAIL_USER,
    mailPass: process.env.MAIL_PASS,
    accessKey: process.env.AWS_ACCESS_KEY_ID ?? '',
    secret: process.env.AWS_SECRET_ACCESS_KEY ?? '',
    bucket: process.env.S3_BUCKET ?? '',
    bucketUtils: process.env.S3_BUCKET_UTILS ?? ''
  }
}

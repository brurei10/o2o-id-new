import { app } from '@/main/config/app'
import { SQLConnection } from '@/infra/db'
import { AccessCodeModel, FeatureModel, Oauth2ClientDetailsModel, ProductServiceModel, UserModel } from '@/domain/models'

import { mockInsertAccessCode, mockInsertFeature, mockInsertOauth2ClientDetails, mockInsertProductService, mockInsertUser } from '@/infra/test'
import request from 'supertest'
import { mockAccountParams, mockProfileParams, mockUserParams } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('User Routes', () => {
  let feature: FeatureModel
  let product: ProductServiceModel
  let user: UserModel
  let accessCode: AccessCodeModel
  let oauth: Oauth2ClientDetailsModel

  const makeParams = (product, feature): any => ({
    ...mockUserParams(),
    product: {
      id: product.id,
      accounts: [
        {
          ...mockAccountParams(),
          productName: 'o2o_APP',
          productServiceId: product.id,
          profiles: [
            { ...mockProfileParams(), features: [feature] }
          ]
        }
      ]
    }

  })

  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
    oauth = await mockInsertOauth2ClientDetails()
    product = await mockInsertProductService()
    feature = await mockInsertFeature(product.id)
    user = await mockInsertUser()
    accessCode = await mockInsertAccessCode()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  describe('POST /oauth/token', () => {
    it('should return 200 with login/email', async () => {
      const params = makeParams(product, feature)
      const token = Buffer.from(`${oauth.clientId}:${oauth.clientSecret}`, 'utf8').toString('base64')

      const { status, body } = await request(app)
        .post('/api/user')
        .send(params)

      const { status: statusCode, body: data } = await request(app)
        .post('/api/oauth/token')
        .set('authorization', `Basic ${token}`)
        .query({
          login: body.email,
          password: params.password,
          grantType: oauth.grantTypes,
          product: product.id
        })

      expect(status).toBe(200)
      expect(statusCode).toBe(200)
      expect(body).toBeDefined()
      expect(data).toBeDefined()
    })

    it('should return 404 if not found', async () => {
      const { status, body } = await request(app)
        .post('/oauth/token')
        .send({})

      expect(status).toBe(404)
      expect(body).toBeDefined()
    })

    it('should return 400 with incorrect input', async () => {
      const { status, body } = await request(app)
        .post('/api/oauth/token')
        .send({})

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })
  })

  describe('POST /auth/reset-password', () => {
    it('should return 200 on send mail reset password', async () => {
      const { body } = await request(app)
        .post('/api/user')
        .send(makeParams(product, feature))

      const { status } = await request(app)
        .post('/api/auth/reset-password')
        .send({ email: body.email })

      expect(status).toBe(200)
    })

    it('should return 404 if not found', async () => {
      const { status, body } = await request(app)
        .post('/auth/reset-password')
        .send({})

      expect(status).toBe(404)
      expect(body).toBeDefined()
    })
  })

  describe('POST /auth/confirm-reset-password', () => {
    it('should return 200 on confirm reset password', async () => {
      const { status, body } = await request(app)
        .post('/api/auth/confirm-reset-password')
        .send(({
          email: accessCode.user.email,
          code: accessCode.code
        }))

      expect(status).toBe(200)
      expect(body).toBeDefined()
    })

    it('should return 401 if confirm reset password fails', async () => {
      const { status } = await request(app)
        .post('/api/auth/confirm-reset-password')
        .send(({
          email: user.email,
          code: 'any_code'
        }))

      expect(status).toBe(401)
    })

    it('should return 404 if not found', async () => {
      const { status, body } = await request(app)
        .post('/auth/confirm-reset-password')
        .send({})

      expect(status).toBe(404)
      expect(body).toBeDefined()
    })
  })

  describe('POST /auth/update-password', () => {
    it('should return 200 on confirm reset password', async () => {
      const password = faker.internet.password()
      const code = await mockInsertAccessCode()

      const { status, body } = await request(app)
        .post('/api/auth/confirm-reset-password')
        .send(({
          email: code.user.email,
          code: code.code
        }))

      const { status: statusCode, body: data } = await request(app)
        .post('/api/auth/update-password')
        .send(({
          email: body.email,
          resetToken: body.resetToken,
          password
        }))

      expect(status).toBe(200)
      expect(statusCode).toBe(200)
      expect(body).toBeDefined()
      expect(data).toBeDefined()
    })

    it('should return 404 if not found', async () => {
      const { status, body } = await request(app)
        .post('/auth/auth/update-password')
        .send({})

      expect(status).toBe(404)
      expect(body).toBeDefined()
    })
  })
})

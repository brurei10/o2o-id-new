import { Router } from 'express'
import { adaptExpressRoute } from '@/main/adapters'
import {
  makeAddUserController
} from '@/main/factories'

export default (router: Router): void => {
  router.post('/user', adaptExpressRoute(makeAddUserController()))
}

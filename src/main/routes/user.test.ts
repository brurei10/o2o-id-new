import { app } from '@/main/config/app'
import { SQLConnection } from '@/infra/db'
import { FeatureModel, ProductServiceModel, UserModel } from '@/domain/models'

import { mockInsertFeature, mockInsertProductService, mockInsertUser } from '@/infra/test'
import request from 'supertest'
import { mockAccountParams, mockProfileParams, mockUserParams } from '@/domain/test'
import { faker } from '@faker-js/faker'

describe('User Routes', () => {
  let feature: FeatureModel
  let product: ProductServiceModel
  let user: UserModel

  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
    user = await mockInsertUser()
    product = await mockInsertProductService()
    feature = await mockInsertFeature(product.id)
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  describe('POST /user', () => {
    it('should return 200 with accessToken', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({
          ...mockUserParams(),
          product: {
            id: product.id,
            accounts: [
              {
                ...mockAccountParams(),
                productName: 'o2o_APP',
                productServiceId: product.id,
                profiles: [
                  { ...mockProfileParams(), features: [feature] }
                ]
              }
            ]
          }
        })

      expect(status).toBe(200)
      expect(body).toBeDefined()
    })

    it('should return 404 if not found', async () => {
      const { status, body } = await request(app)
        .post('/user')
        .send({})

      expect(status).toBe(404)
      expect(body).toBeDefined()
    })

    it('should return 400 with incorrect input', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({})

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })

    it('should return 400 with incorrect cpf', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({
          ...mockUserParams(),
          cpfCnpj: faker.random.alphaNumeric(),
          product: {
            id: product.id,
            accounts: [
              {
                ...mockAccountParams(),
                productName: 'o2o_APP',
                productServiceId: product.id,
                profiles: [
                  { ...mockProfileParams(), features: [feature] }
                ]
              }
            ]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })

    it('should return 400 with incorrect product', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({
          ...mockUserParams(),
          product: {
            id: faker.datatype.number(),
            accounts: [
              {
                ...mockAccountParams(),
                productName: 'o2o_APP',
                productServiceId: product.id,
                profiles: [
                  { ...mockProfileParams(), features: [feature] }
                ]
              }
            ]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })

    it('should return 400 with incorrect feature', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({
          ...mockUserParams(),
          product: {
            id: product.id,
            accounts: [
              {
                ...mockAccountParams(),
                productName: 'o2o_APP',
                productServiceId: product.id,
                profiles: [
                  { ...mockProfileParams(), features: [{ ...feature, id: faker.datatype.number() }] }
                ]
              }
            ]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })

    it('should return 400 if user exists', async () => {
      const { status, body } = await request(app)
        .post('/api/user')
        .send({
          ...mockUserParams(),
          cpfCnpj: user.cpfCnpj,
          product: {
            id: product.id,
            accounts: [
              {
                ...mockAccountParams(),
                productName: 'o2o_APP',
                productServiceId: product.id,
                profiles: [
                  { ...mockProfileParams(), features: [feature] }
                ]
              }
            ]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeDefined()
    })
  })
})

import { Router } from 'express'
import { adaptExpressRoute } from '@/main/adapters'
import {
  makeAuthenticationController, makeConfirmResetPasswordController, makeResetPasswordController, makeUpdatePasswordController
} from '@/main/factories'

export default (router: Router): void => {
  router.post('/oauth/token', adaptExpressRoute(makeAuthenticationController()))

  router.post('/auth/reset-password', adaptExpressRoute(makeResetPasswordController()))
  router.post('/auth/confirm-reset-password', adaptExpressRoute(makeConfirmResetPasswordController()))
  router.post('/auth/update-password', adaptExpressRoute(makeUpdatePasswordController()))
}

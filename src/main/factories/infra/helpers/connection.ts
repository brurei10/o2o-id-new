import { SQLConnection } from '@/infra/db'

export const makeSqlConnection = (): SQLConnection => {
  return SQLConnection.getInstance()
}

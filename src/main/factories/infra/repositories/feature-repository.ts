import { FeatureRepository } from '@/infra/db'

export const makeFeatureRepository = (): FeatureRepository => {
  return new FeatureRepository()
}

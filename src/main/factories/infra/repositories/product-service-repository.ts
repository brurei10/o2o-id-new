import { ProductServiceRepository } from '@/infra/db'

export const makeProductServiceRepository = (): ProductServiceRepository => {
  return new ProductServiceRepository()
}

import { AccessCodeRepository } from '@/infra/db'

export const makeAccessCodeRepository = (): AccessCodeRepository => {
  return new AccessCodeRepository()
}

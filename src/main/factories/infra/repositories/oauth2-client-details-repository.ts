import { Oauth2ClientDetailsRepository } from '@/infra/db'

export const makeOauth2ClientDetailsRepository = (): Oauth2ClientDetailsRepository => {
  return new Oauth2ClientDetailsRepository()
}

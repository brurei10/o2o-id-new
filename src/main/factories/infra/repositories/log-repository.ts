import { LogRepository } from '@/infra/db'

export const makeLogRepository = (): LogRepository => {
  return new LogRepository()
}

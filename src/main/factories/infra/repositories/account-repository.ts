import { AccountRepository } from '@/infra/db'

export const makeAccountRepository = (): AccountRepository => {
  return new AccountRepository()
}

import { ProfileRepository } from '@/infra/db'

export const makeProfileRepository = (): ProfileRepository => {
  return new ProfileRepository()
}

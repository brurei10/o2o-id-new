import { UserRepository } from '@/infra/db'

export const makeUserRepository = (): UserRepository => {
  return new UserRepository()
}

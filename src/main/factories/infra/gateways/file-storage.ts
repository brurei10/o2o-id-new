import { env } from '@/main/config/env'
import { AwsS3FileStorage } from '@/infra/gateways'

export const makeAwsS3FileStorage = (bucket: string): AwsS3FileStorage => {
  return new AwsS3FileStorage(
    env.aws.accessKey,
    env.aws.secret,
    bucket
  )
}

import { CpfCnpjAdapter } from '@/infra/gateways'

export const makeCpfCnpjAdapter = (): CpfCnpjAdapter => {
  return new CpfCnpjAdapter()
}

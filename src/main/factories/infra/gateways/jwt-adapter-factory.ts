import { JwtAdapter } from '@/infra/gateways'

export const makeJwtAdapter = (): JwtAdapter => {
  return new JwtAdapter()
}

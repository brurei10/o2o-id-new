import { MailProviderAdapter } from '@/infra/gateways'
import { env } from '@/main/config/env'
import { makeAwsS3FileStorage } from '@/main/factories'

export const makeMailProviderAdapter = (): MailProviderAdapter => {
  return new MailProviderAdapter(makeAwsS3FileStorage(env.aws.bucketUtils))
}

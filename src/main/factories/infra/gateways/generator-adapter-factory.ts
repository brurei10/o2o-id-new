import { GeneratorAdapter } from '@/infra/gateways'

export const makeGeneratorAdapter = (): GeneratorAdapter => {
  const min = 10000
  const max = 99999
  return new GeneratorAdapter(min, max)
}

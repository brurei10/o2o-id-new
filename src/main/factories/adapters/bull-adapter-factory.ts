import { Worker } from '@/application/protocols'
import { BullAdapter } from '@/main/adapters'
import { makeLogRepository } from '@/main/factories'

export const makeBullAdapter = (name: string, worker: Worker): BullAdapter => {
  return new BullAdapter(name, worker, makeLogRepository())
}

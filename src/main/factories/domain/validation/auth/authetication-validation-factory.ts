import { AuthValidation } from '@/domain/validation'
import { makeBcryptAdapter, makeUserRepository, makeProductServiceRepository } from '@/main/factories'

export const makeAuthValidation = (): AuthValidation => {
  return new AuthValidation(makeProductServiceRepository(), makeUserRepository(), makeBcryptAdapter())
}

import { UpdatePasswordValidation } from '@/domain/validation'
import { makeUserRepository, makeJwtAdapter } from '@/main/factories'

export const makeUpdatePasswordValidation = (): UpdatePasswordValidation => {
  return new UpdatePasswordValidation(makeUserRepository(), makeJwtAdapter())
}

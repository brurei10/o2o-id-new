import { ConfirmResetPasswordValidation } from '@/domain/validation'
import { makeUserRepository, makeAccessCodeRepository } from '@/main/factories'

export const makeConfirmResetPasswordValidation = (): ConfirmResetPasswordValidation => {
  return new ConfirmResetPasswordValidation(makeAccessCodeRepository(), makeUserRepository())
}

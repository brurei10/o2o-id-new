import { ResetPasswordValidation } from '@/domain/validation'
import { makeUserRepository } from '@/main/factories'

export const makeResetPasswordValidation = (): ResetPasswordValidation => {
  return new ResetPasswordValidation(makeUserRepository())
}

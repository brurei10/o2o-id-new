import { DbAddUserValidation } from '@/domain/validation'
import { makeProductServiceRepository, makeFeatureRepository, makeCpfCnpjAdapter, makeUserRepository } from '@/main/factories'

export const makeDbAddUserValidation = (): DbAddUserValidation => {
  return new DbAddUserValidation(makeProductServiceRepository(), makeFeatureRepository(), makeCpfCnpjAdapter(), makeUserRepository())
}

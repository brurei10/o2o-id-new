import { AddUserService } from '@/domain/services'
import { makeUserRepository, makeAccountRepository, makeProductServiceRepository, makeProfileRepository, makeBcryptAdapter } from '@/main/factories'

export const makeAddUserService = (): AddUserService => {
  return new AddUserService(
    makeProductServiceRepository(),
    makeUserRepository(),
    makeAccountRepository(),
    makeProfileRepository(),
    makeBcryptAdapter()
  )
}

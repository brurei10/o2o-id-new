import { SendMailService } from '@/domain/services'
import { makeMailProviderAdapter } from '@/main/factories'

export const makeSendMailService = (): SendMailService => {
  return new SendMailService(makeMailProviderAdapter())
}

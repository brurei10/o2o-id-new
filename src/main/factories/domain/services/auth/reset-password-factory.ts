import { ResetPasswordService } from '@/domain/services'
import { makeUserRepository, makeSendMailWorker, makeGeneratorAdapter, makeAccessCodeRepository } from '@/main/factories'

export const makeResetPasswordService = (): ResetPasswordService => {
  return new ResetPasswordService(makeUserRepository(), makeGeneratorAdapter(), makeAccessCodeRepository(), makeSendMailWorker())
}

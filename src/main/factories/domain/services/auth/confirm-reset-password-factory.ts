import { ConfirmResetPasswordService } from '@/domain/services'
import { makeJwtAdapter, makeAccessCodeRepository } from '@/main/factories'

export const makeConfirmResetPasswordService = (): ConfirmResetPasswordService => {
  return new ConfirmResetPasswordService(makeAccessCodeRepository(), makeJwtAdapter())
}

import { AuthenticationService } from '@/domain/services'
import { makeUserRepository, makeJwtAdapter, makeOauth2ClientDetailsRepository } from '@/main/factories'

export const makeAuthenticationService = (): AuthenticationService => {
  return new AuthenticationService(
    makeUserRepository(),
    makeOauth2ClientDetailsRepository(),
    makeJwtAdapter()
  )
}

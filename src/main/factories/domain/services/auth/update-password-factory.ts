import { UpdatePasswordService } from '@/domain/services'
import { makeUserRepository, makeBcryptAdapter } from '@/main/factories'

export const makeUpdatePasswordService = (): UpdatePasswordService => {
  return new UpdatePasswordService(makeUserRepository(), makeBcryptAdapter())
}

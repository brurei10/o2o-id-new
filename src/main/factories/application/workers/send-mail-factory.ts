import { SendMailWorker } from '@/application/workers'
import { makeSendMailService, makeBullAdapter } from '@/main/factories'
import { BullAdapter } from '@/main/adapters'

export const makeSendMailWorker = (): BullAdapter => {
  const sendMailWorker = new SendMailWorker(makeSendMailService())
  return makeBullAdapter('send_mail', sendMailWorker)
}

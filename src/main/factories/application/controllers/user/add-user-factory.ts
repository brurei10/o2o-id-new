import { AddUserController } from '@/application/controllers'
import { Controller } from '@/application/protocols'
import { makeAddUserService, makeSqlTransactionController, makeDbAddUserValidation } from '@/main/factories'

export const makeAddUserController = (): Controller => {
  const controller = new AddUserController(
    makeDbAddUserValidation(),
    makeAddUserService()
  )
  return makeSqlTransactionController(controller)
}

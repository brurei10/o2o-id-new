import { AuthenticationController } from '@/application/controllers'
import { Controller } from '@/application/protocols'
import { makeAuthenticationService, makeAuthValidation, makeSqlTransactionController } from '@/main/factories'

export const makeAuthenticationController = (): Controller => {
  const controller = new AuthenticationController(
    makeAuthValidation(),
    makeAuthenticationService()
  )
  return makeSqlTransactionController(controller)
}

import { ResetPasswordController } from '@/application/controllers'
import { Controller } from '@/application/protocols'
import { makeResetPasswordService, makeSqlTransactionController, makeResetPasswordValidation } from '@/main/factories'

export const makeResetPasswordController = (): Controller => {
  const resetPasswordController = new ResetPasswordController(makeResetPasswordValidation(), makeResetPasswordService())
  return makeSqlTransactionController(resetPasswordController)
}

import { UpdatePasswordController } from '@/application/controllers'
import { Controller } from '@/application/protocols'
import { makeUpdatePasswordService, makeUpdatePasswordValidation, makeSqlTransactionController } from '@/main/factories'

export const makeUpdatePasswordController = (): Controller => {
  const updatePasswordController = new UpdatePasswordController(makeUpdatePasswordValidation(), makeUpdatePasswordService())
  return makeSqlTransactionController(updatePasswordController)
}

import { ConfirmResetPasswordController } from '@/application/controllers'
import { Controller } from '@/application/protocols'
import { makeConfirmResetPasswordService, makeConfirmResetPasswordValidation, makeSqlTransactionController } from '@/main/factories'

export const makeConfirmResetPasswordController = (): Controller => {
  const confirmResetPasswordController = new ConfirmResetPasswordController(makeConfirmResetPasswordValidation(), makeConfirmResetPasswordService())
  return makeSqlTransactionController(confirmResetPasswordController)
}

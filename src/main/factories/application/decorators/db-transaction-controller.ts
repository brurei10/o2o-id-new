import { MainController } from '@/application/controllers'
import { DbTransactionController } from '@/application/decorators'
import { Controller } from '@/application/protocols'
import { makeSqlConnection, makeLogRepository } from '@/main/factories'

export const makeSqlTransactionController = (mainController: MainController): Controller => {
  return new DbTransactionController(mainController, makeSqlConnection(), makeLogRepository())
}

import { mockProductServiceParams } from '@/domain/test'
import { mapper, ProductService } from '@/infra/db'

import { getRepository } from 'typeorm'
import { ProductServiceModel } from '@/domain/models'

export const mockInsertProductService = async (): Promise<ProductServiceModel> => {
  const product = await getRepository(ProductService).save(mockProductServiceParams())
  return product && mapper(product)
}

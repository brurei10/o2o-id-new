import { mapper, AccessCode } from '@/infra/db'

import { getRepository } from 'typeorm'
import { AccessCodeModel } from '@/domain/models'
import { mockInsertUser } from './mock-user'
import { mockAccessCodeParams } from '@/domain/test'

export const mockInsertAccessCode = async (): Promise<AccessCodeModel> => {
  const user = await mockInsertUser()
  const today = new Date()
  const validAt = (new Date(today.setHours(today.getHours() + 4)))
  const userAcess = await getRepository(AccessCode).save({ ...mockAccessCodeParams(), userId: user.id, validAt })
  return userAcess && mapper({ ...userAcess, user })
}

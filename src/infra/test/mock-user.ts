import { mockUserParams } from '@/domain/test'
import { mapper, User } from '@/infra/db'

import { getRepository } from 'typeorm'
import { UserModel } from '@/domain/models'
import { mockInsertProductService } from './mock-product-service'

export const mockInsertUser = async (): Promise<UserModel> => {
  const product = await mockInsertProductService()
  const user = await getRepository(User).save({ ...mockUserParams(), productServices: [product] })
  return user && mapper(user)
}

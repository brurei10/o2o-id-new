import { mockFeatureParams } from '@/domain/test'
import { mapper, Feature } from '@/infra/db'

import { getRepository } from 'typeorm'
import { FeatureModel } from '@/domain/models'
import { mockInsertProductService } from './mock-product-service'

export const mockInsertFeature = async (productServiceId?: number): Promise<FeatureModel> => {
  const product = await mockInsertProductService()
  const feature = await getRepository(Feature).save({ ...mockFeatureParams(), productServiceId: productServiceId ?? product.id })
  return feature && mapper(feature)
}

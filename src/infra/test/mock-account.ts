import { mockAccountParams } from '@/domain/test'
import { mapper, Account } from '@/infra/db'

import { getRepository } from 'typeorm'
import { AccountModel } from '@/domain/models'
import { mockInsertProductService } from './mock-product-service'
import { mockInsertUser } from './mock-user'

export const mockInsertAccount = async (): Promise<AccountModel> => {
  const product = await mockInsertProductService()
  const user = await mockInsertUser()
  const account = await getRepository(Account).save({ ...mockAccountParams(), productServiceId: product.id, userOwnerId: user.id })
  return account && mapper(account)
}

import { mockOauth2ClientDetailsParams } from '@/domain/test'
import { mapper, Oauth2ClientDetails } from '@/infra/db'

import { getRepository } from 'typeorm'
import { Oauth2ClientDetailsModel } from '@/domain/models'

export const mockInsertOauth2ClientDetails = async (): Promise<Oauth2ClientDetailsModel> => {
  const oauth = await getRepository(Oauth2ClientDetails).save(mockOauth2ClientDetailsParams())
  return oauth && mapper(oauth)
}

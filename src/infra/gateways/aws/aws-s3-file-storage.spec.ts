import { AwsS3FileStorage } from './aws-s3-file-storage'

import { config, S3 } from 'aws-sdk'
import { mocked } from 'jest-mock'

jest.mock('aws-sdk')

describe('AwsS3FileStorage', () => {
  let accessKey: string
  let secret: string
  let key: string
  let bucket: string
  let sut: AwsS3FileStorage

  beforeAll(() => {
    accessKey = 'any_access_key'
    secret = 'any_secret'
    bucket = 'any_bucket'
    key = 'any_key_name'
  })

  beforeEach(() => {
    sut = new AwsS3FileStorage(accessKey, secret, bucket)
  })

  it('should config aws credentials on creation', () => {
    expect(sut).toBeDefined()
    expect(config.update).toHaveBeenCalledWith({
      credentials: {
        accessKeyId: accessKey,
        secretAccessKey: secret
      }
    })
    expect(config.update).toHaveBeenCalledTimes(1)
  })

  describe('load', () => {
    let loadObjectPromiseSpy: jest.Mock
    let loadObjectSpy: jest.Mock

    beforeAll(() => {
      loadObjectPromiseSpy = jest.fn()
      loadObjectSpy = jest.fn().mockImplementation(() => ({ promise: loadObjectPromiseSpy }))
      mocked(S3).mockImplementation(jest.fn().mockImplementation(() => ({ getObject: loadObjectSpy })))
    })

    it('should rethrow if loadObject throws', async () => {
      const error = new Error('load_error')
      loadObjectPromiseSpy.mockRejectedValueOnce(error)

      const promise = sut.load({ key })

      await expect(promise).rejects.toThrow(error)
    })
  })
})

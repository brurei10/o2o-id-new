import { LoadFile } from '@/domain/protocols'

import { config, S3 } from 'aws-sdk'

export class AwsS3FileStorage implements LoadFile {
  constructor (accessKey: string, secret: string, private readonly bucket: string) {
    config.update({
      credentials: {
        accessKeyId: accessKey,
        secretAccessKey: secret
      }
    })
  }

  async load ({ key }: LoadFile.Input): Promise<LoadFile.Output> {
    const { Body } = await new S3().getObject({
      Bucket: this.bucket,
      Key: key
    }).promise()
    return Body
  }
}

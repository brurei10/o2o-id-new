import { faker } from '@faker-js/faker'
import { cnpj, cpf } from 'cpf-cnpj-validator'
import { CpfCnpjAdapter } from './cpf-cnpj-validator'

const makeSut = (): CpfCnpjAdapter => new CpfCnpjAdapter()

describe('CpfCnpjAdapter', () => {
  let validCnpj: string
  let validCpf: string
  let invalidCpf: string
  let invalidCnpj: string

  beforeAll(() => {
    invalidCpf = faker.random.numeric()
    invalidCnpj = faker.random.numeric()
    validCpf = cpf.generate()
    validCnpj = cnpj.generate()
  })
  it('should return a InvalidParamError if cpf validation fails', async () => {
    const sut = makeSut()
    const error = await sut.cpfValid({ value: invalidCpf })
    expect(error).toBeFalsy()
  })

  it('should return if cpf validation succeds', async () => {
    const sut = makeSut()
    const error = await sut.cpfValid({ value: validCpf })
    expect(error).toBeTruthy()
  })

  it('should return a  InvalidParamError if cnpj validation fails', async () => {
    const sut = makeSut()
    const error = await sut.cnpjValid({ value: invalidCnpj })
    expect(error).toBeFalsy()
  })

  it('should return if validation cnpj succeds', async () => {
    const sut = makeSut()
    const error = await sut.cnpjValid({ value: validCnpj })
    expect(error).toBeTruthy()
  })
})

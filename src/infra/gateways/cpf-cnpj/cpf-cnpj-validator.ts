import { cnpj, cpf } from 'cpf-cnpj-validator'
import { CnpjValidation, CpfValidation } from '@/domain/protocols'

export class CpfCnpjAdapter implements CnpjValidation, CpfValidation {
  async cnpjValid ({ value }: CnpjValidation.Input): Promise<CnpjValidation.Output> {
    return cnpj.isValid(value)
  }

  async cpfValid ({ value }: CnpjValidation.Input): Promise<CnpjValidation.Output> {
    return cpf.isValid(value)
  }
}

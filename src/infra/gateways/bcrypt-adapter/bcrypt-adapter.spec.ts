import { HashComparer } from '@/domain/protocols'
import { faker } from '@faker-js/faker'
import bcrypt from 'bcrypt'
import { BcryptAdapter } from './bcrypt-adapter'

jest.mock('bcrypt', () => ({
  async hash (): Promise<string> {
    return await Promise.resolve('hash')
  },

  async compare (): Promise<boolean> {
    return await Promise.resolve(true)
  }
}))

describe('Bcrypt Adapter', () => {
  let sut: BcryptAdapter
  let params: HashComparer.Input
  let salt: number

  beforeAll(() => {
    salt = 12
    params = {
      value: faker.database.column(),
      hash: faker.datatype.uuid()
    }
  })

  beforeEach(() => {
    sut = new BcryptAdapter(salt)
  })

  it('Should call hash with correct values', async () => {
    const hashSpy = jest.spyOn(bcrypt, 'hash')
    await sut.hash({ value: 'any_value' })
    expect(hashSpy).toHaveBeenCalledWith('any_value', salt)
  })

  it('Should return a valid hash on success', async () => {
    const hash = await sut.hash({ value: 'any_value' })
    expect(hash).toBe('hash')
  })

  it('Should throw if hash throws', async () => {
    jest.spyOn(bcrypt, 'hash').mockImplementationOnce(async () => { return await Promise.reject(new Error()) })
    const promise = sut.hash({ value: 'any_value' })
    await expect(promise).rejects.toThrow()
  })

  it('Should call compare with correct values', async () => {
    const compareSpy = jest.spyOn(bcrypt, 'compare')
    await sut.compare(params)
    expect(compareSpy).toHaveBeenCalledWith(params.value, params.hash)
  })

  it('Should return true if compare succeds', async () => {
    const isValid = await sut.compare(params)
    expect(isValid).toBe(true)
  })

  it('Should return false if compare fails', async () => {
    jest.spyOn(bcrypt, 'compare').mockImplementationOnce(async () => { return await Promise.resolve(false) })
    const isValid = await sut.compare(params)
    expect(isValid).toBe(false)
  })

  it('Should throw if compare throws', async () => {
    jest.spyOn(bcrypt, 'compare').mockImplementationOnce(async () => { return await Promise.reject(new Error()) })
    const promise = sut.compare(params)
    await expect(promise).rejects.toThrow()
  })
})

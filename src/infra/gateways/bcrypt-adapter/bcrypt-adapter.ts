import { HashComparer, Hasher } from '@/domain/protocols'
import bcrypt from 'bcrypt'

export class BcryptAdapter implements Hasher, HashComparer {
  constructor (private readonly salt: number) {}
  async hash ({ value }: Hasher.Input): Promise<Hasher.Output> {
    const hash = await bcrypt.hash(value, this.salt)
    return hash
  }

  async compare ({ value, hash }: HashComparer.Input): Promise<HashComparer.Output> {
    const isValid = await bcrypt.compare(value, hash)
    return isValid
  }
}

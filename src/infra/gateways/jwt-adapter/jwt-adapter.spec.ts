import { env } from '@/main/config'
import { faker } from '@faker-js/faker'
import jwt from 'jsonwebtoken'
import { JwtAdapter } from './jwt-adapter'

jest.mock('jsonwebtoken')

describe('Jwt Adapter', () => {
  let value: string
  let decrypted: string
  let token: string
  let sut: JwtAdapter
  let fakeJwt: jest.Mocked<typeof jwt>

  beforeAll(() => {
    token = faker.datatype.uuid()
    value = faker.datatype.uuid()
    decrypted = faker.datatype.json()
    fakeJwt = jwt as jest.Mocked<typeof jwt>
    fakeJwt.sign.mockImplementation(() => token)
    fakeJwt.verify.mockImplementation(() => decrypted)
  })

  beforeEach(() => {
    sut = new JwtAdapter()
  })

  it('should call sign with correct params', async () => {
    await sut.encrypt({ value })

    expect(fakeJwt.sign).toHaveBeenCalledWith({ value }, env.accessTokenSecret, { expiresIn: env.accessTokenExpiry })
    expect(fakeJwt.sign).toHaveBeenCalledWith({ value }, env.refreshTokenSecret, { expiresIn: env.refeshTokenExpiry })
  })

  it('should return a token', async () => {
    const jwtToken = await sut.encrypt({ value })

    expect(jwtToken).toEqual({
      access_token: token,
      refresh_token: token,
      expires_in: env.accessTokenExpiry
    })
  })

  it('should rethrow if jwt throws', async () => {
    fakeJwt.sign.mockImplementationOnce(() => { throw new Error('token_error') })

    const promise = sut.encrypt({ value })

    await expect(promise).rejects.toThrow(new Error('token_error'))
  })

  it('should call verify with correct params', async () => {
    await sut.decrypt({ value })

    expect(fakeJwt.verify).toHaveBeenCalledWith(value, env.accessTokenSecret)
    expect(fakeJwt.verify).toHaveBeenCalledWith(value, env.accessTokenSecret)
  })

  it('should return a correct value decrypted', async () => {
    const result = await sut.decrypt({ value })

    expect(result).toEqual(decrypted)
  })

  it('should return null if jwt throws', async () => {
    fakeJwt.verify.mockImplementationOnce(() => { throw new Error('token_error') })

    const result = await sut.decrypt({ value })
    expect(result).toEqual(null)
  })
})

import { Decrypter, Encrypter } from '@/domain/protocols'
import { env } from '@/main/config'
import jwt from 'jsonwebtoken'

export class JwtAdapter implements Encrypter, Decrypter {
  async encrypt ({ value }: Encrypter.Input): Promise<Encrypter.Output> {
    const accessToken = jwt.sign({ value }, env.accessTokenSecret, {
      expiresIn: env.accessTokenExpiry
    })
    const refeshToken = jwt.sign({ value }, env.refreshTokenSecret, {
      expiresIn: env.refeshTokenExpiry
    })
    return {
      access_token: accessToken,
      refresh_token: refeshToken,
      expires_in: env.accessTokenExpiry
    }
  }

  async decrypt ({ value: token }: Decrypter.Input): Promise<Decrypter.Output> {
    try {
      const value: any = jwt.verify(token, env.accessTokenSecret)
      return value
    } catch (error) {
      return null
    }
  }
}

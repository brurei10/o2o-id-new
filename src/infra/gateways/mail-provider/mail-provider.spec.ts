import { MailProviderAdapter } from './mail-provider'
import { LoadFile, MailProvider } from '@/domain/protocols'

import { faker } from '@faker-js/faker'
import { mock, MockProxy } from 'jest-mock-extended'

describe('MailProvider Adapter', () => {
  let loadFile: MockProxy<LoadFile>
  let sut: MailProviderAdapter
  let input: MailProvider.Input

  beforeAll(() => {
    input = {
      from: faker.internet.email(),
      to: faker.internet.email(),
      subject: faker.random.words(),
      viewPath: faker.internet.url(),
      data: {
        value: faker.random.word()
      }
    }
    loadFile = mock()
    loadFile.load.mockResolvedValue(Buffer.from('any_value'))
  })

  beforeEach(() => {
    sut = new MailProviderAdapter(loadFile)
  })

  it('should throw if LoadFile throws', async () => {
    loadFile.load.mockRejectedValueOnce(new Error('load_error'))
    const promise = sut.sendMail(input)
    await expect(promise).rejects.toThrow(new Error('load_error'))
  })
})

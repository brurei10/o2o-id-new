import { LoadFile, MailProvider } from '@/domain/protocols'
import { env } from '@/main/config/env'

import Mail from 'nodemailer/lib/mailer'
import nodemailer from 'nodemailer'
import handlebars from 'handlebars'

export class MailProviderAdapter implements MailProvider {
  private readonly transporter: Mail
  constructor (
    private readonly loadFile: LoadFile
  ) {
    this.transporter = nodemailer.createTransport({
      host: env.aws.mailHost,
      port: parseInt(env.aws.mailPort),
      auth: {
        user: env.aws.mailUser,
        pass: env.aws.mailPass
      },
      tls: {
        rejectUnauthorized: false
      }
    })
  }

  async sendMail (message: MailProvider.Input): Promise<MailProvider.Output> {
    const template = await this.loadTemplate(message.data, message.viewPath)
    const reponse = await this.transporter.sendMail({ ...message, html: template })
    return reponse
  }

  private async loadTemplate (input: any, path: string): Promise<string> {
    const Body = await this.loadFile.load({ key: 'views/' + `${path}.hbs` })
    return handlebars.compile(Body.toString('utf-8'))({ ...input, support_link: 'https://www.o2osaude.com.br' })
  }
}

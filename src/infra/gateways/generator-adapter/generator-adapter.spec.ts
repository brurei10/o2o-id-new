import { GeneratorAdapter } from './generator-adapter'

jest.mock('crypto', () => ({
  randomInt (): number {
    return 123456
  }
}))

describe('Generator Adapter', () => {
  let sut: GeneratorAdapter
  let min: number
  let max: number

  beforeAll(() => {
    min = 100000
    max = 999999
  })

  beforeEach(() => {
    sut = new GeneratorAdapter(min, max)
  })

  it('Should return a code on success', async () => {
    const result = await sut.generate()

    expect(result).toBe('123456')
  })
})

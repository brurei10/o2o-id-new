import { CodeGenerator } from '@/domain/protocols'
import crypto from 'crypto'

export class GeneratorAdapter implements CodeGenerator {
  constructor (
    private readonly min: number,
    private readonly max: number
  ) { }

  async generate (): Promise<string> {
    return crypto.randomInt(this.min, this.max).toString()
  }
}

import { Entity as DbEntity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm'

@DbEntity('tb_logs')
export class Log {
  constructor (input: { content: string, type: string }) {
    Object.assign(this, input)
  }

  @PrimaryGeneratedColumn()
    id?: number

  @Column()
    content: string

  @Column()
    type: string

  @CreateDateColumn({ name: 'created_at' })
    createdAt: Date
}

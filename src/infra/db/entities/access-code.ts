import { Entity } from './entity'
import { AccessCodeModel } from '@/domain/models'
import { User } from './user'

import { Entity as DbEntity, Column, JoinColumn, ManyToOne } from 'typeorm'

@DbEntity('tb_access_codes')
export class AccessCode extends Entity {
  constructor (input: AccessCodeModel) {
    super()
    Object.assign(this, input)
  }

  @Column({ name: 'user_id' })
    userId: number

  @Column()
    code: string

  @Column()
    type: string

  @Column({ name: 'valid_at' })
    validAt: Date

  @ManyToOne(() => User, user => user.accessCodes)
  @JoinColumn({ name: 'user_id' })
    user: User
}

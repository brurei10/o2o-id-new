import { Entity } from './entity'

import { Entity as DbEntity, Column, CreateDateColumn, OneToMany, ManyToMany } from 'typeorm'
import { Account } from './account'
import { UserAccountProfile } from './user-account-profile'
import { AccountUser } from './account-user'
import { ProductService } from './product-service'
import { UserModel } from '@/domain/models'
import { AccessCode } from './access-code'

@DbEntity('tb_users')
export class User extends Entity {
  constructor (input: UserModel) {
    super()
    Object.assign(this, input)
  }

  @Column()
    name: string

  @Column()
    email: string

  @Column()
    phone: string

  @Column()
    password: string

  @Column({ name: 'cpf_cnpj' })
    cpfCnpj: string

  @Column({ name: 'id_external' })
    idExternal: string

  @Column()
    login: string

  @Column({ name: 'whats_phone' })
    whatsPhone: string

  @Column({ name: 'user_language' })
    userLanguage: string

  @Column()
    token: string

  @Column({ name: 'first_access' })
    firstAccess: boolean

  @Column()
    active: boolean

  @CreateDateColumn({ name: 'last_access' })
    lastAccess: Date

  @OneToMany(() => Account, account => account.userOwner)
    accounts: Account[]

  @OneToMany(() => AccountUser, accountUser => accountUser.user)
    accountUsers: AccountUser[]

  @OneToMany(() => UserAccountProfile, userAccountProfile => userAccountProfile.user)
    userAccountProfiles: UserAccountProfile[]

  @OneToMany(() => AccessCode, accessCode => accessCode.user)
    accessCodes: AccessCode[]

  @ManyToMany(() => ProductService, productService => productService.users)
    productServices?: ProductService[]
}

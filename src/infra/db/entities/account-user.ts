import { Entity } from './entity'

import { Entity as DbEntity, Column, JoinColumn, ManyToOne } from 'typeorm'
import { AccountUserModel } from '@/domain/models'
import { Account } from './account'
import { User } from './user'

@DbEntity('tb_account_users')
export class AccountUser extends Entity {
  constructor (input: AccountUserModel) {
    super()
    Object.assign(this, input)
  }

  @Column({ name: 'account_id' })
    accountId: number

  @Column({ name: 'user_id' })
    userId: number

  @Column({ name: 'is_admin' })
    isAdmin: boolean

  @ManyToOne(() => Account, account => account.accountUsers)
  @JoinColumn({ name: 'account_id' })
    account: Account

  @ManyToOne(() => User, user => user.accountUsers)
  @JoinColumn({ name: 'user_id' })
    user: User
}

import { Entity as DbEntity, Column, JoinColumn, ManyToOne, ManyToMany, JoinTable, PrimaryColumn } from 'typeorm'
import { FeatureModel } from '@/domain/models'
import { ProductService } from './product-service'
import { Profile } from './profile'

@DbEntity('tb_features')
export class Feature {
  constructor (input: FeatureModel) {
    Object.assign(this, input)
  }

  @PrimaryColumn()
    id?: number

  @Column({ name: 'product_service_id' })
    productServiceId: number

  @Column()
    name: string

  @ManyToOne(() => ProductService, productService => productService.features)
  @JoinColumn({ name: 'product_service_id' })
    productService: ProductService

  @ManyToMany(() => Profile, profile => profile.features)
  @JoinTable({
    name: 'tb_features_profiles',
    joinColumns: [
      { name: 'feature_id' }
    ],
    inverseJoinColumns: [
      { name: 'profile_id' }
    ]
  })
    profiles?: Profile[]
}

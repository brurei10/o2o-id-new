import { Oauth2ClientDetailsModel } from '@/domain/models'
import { Entity as DbEntity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm'

@DbEntity('tb_oauth2_client_details')
export class Oauth2ClientDetails {
  constructor (input: Oauth2ClientDetailsModel) {
    Object.assign(this, input)
  }

  @PrimaryGeneratedColumn('uuid')
    id?: string

  @Column({ name: 'access_token_validity' })
    accessTokenValidity: number

  @Column()
    authorities: string

  @Column({ name: 'client_id' })
    clientId: string

  @Column({ name: 'client_secret' })
    clientSecret: string

  @Column({ name: 'grant_types' })
    grantTypes: string

  @Column({ name: 'redirect_uris' })
    redirectUris: string

  @Column({ name: 'refresh_token_validity' })
    refreshTokenValidity: number

  @Column({ name: 'resources' })
    resources: string

  @Column({ name: 'scopes' })
    scopes: string

  @Column({ name: 'secret_required' })
    secretRequired: boolean

  @CreateDateColumn({ name: 'created_at' })
    createdAt: Date

  @UpdateDateColumn({ name: 'updated_at' })
    updatedAt: Date
}

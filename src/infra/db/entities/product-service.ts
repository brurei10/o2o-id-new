import { Entity as DbEntity, Column, OneToMany, JoinColumn, ManyToOne, JoinTable, ManyToMany, PrimaryColumn } from 'typeorm'
import { ProductServiceModel } from '@/domain/models'
import { Account } from './account'
import { Profile } from './profile'
import { Feature } from './feature'
import { User } from './user'

@DbEntity('tb_product_services')
export class ProductService {
  constructor (input: ProductServiceModel) {
    Object.assign(this, input)
  }

  @PrimaryColumn()
    id?: number

  @Column({ name: 'owner_product_id' })
    ownerProductId: number

  @Column()
    description: string

  @Column()
    name: string

  @OneToMany(() => Account, account => account.productService)
    accounts: Account[]

  @OneToMany(() => Profile, profile => profile.productService)
    profiles: Profile[]

  @OneToMany(() => ProductService, productService => productService.productService)
    productServices: ProductService[]

  @OneToMany(() => Feature, feature => feature.productService)
    features: Feature[]

  @ManyToOne(() => ProductService, productService => productService.productServices)
  @JoinColumn({ name: 'owner_product_id' })
    productService: ProductService

  @ManyToMany(() => User, user => user.productServices)
  @JoinTable({
    name: 'tb_users_product_services',
    joinColumns: [
      { name: 'product_service_id' }
    ],
    inverseJoinColumns: [
      { name: 'user_id' }
    ]
  })
    users?: User[]
}

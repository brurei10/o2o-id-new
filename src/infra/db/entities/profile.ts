import { Entity } from './entity'

import { Entity as DbEntity, Column, ManyToOne, JoinColumn, OneToMany, JoinTable, ManyToMany } from 'typeorm'
import { ProfileModel } from '@/domain/models'
import { Account } from './account'
import { ProductService } from './product-service'
import { UserAccountProfile } from './user-account-profile'
import { Feature } from './feature'

@DbEntity('tb_profiles')
export class Profile extends Entity {
  constructor (input: ProfileModel) {
    super()
    Object.assign(this, input)
  }

  @Column({ name: 'account_id' })
    accountId: number

  @Column({ name: 'product_service_id' })
    productServiceId: number

  @Column()
    name: string

  @ManyToOne(() => Account, account => account.profiles)
  @JoinColumn({ name: 'account_id' })
    account: Account

  @ManyToOne(() => ProductService, productService => productService.profiles)
  @JoinColumn({ name: 'product_service_id' })
    productService: ProductService

  @OneToMany(() => UserAccountProfile, userAccountProfile => userAccountProfile.profile)
    userAccountProfiles: UserAccountProfile[]

  @ManyToMany(() => Feature, feature => feature.profiles)
  @JoinTable({
    name: 'tb_features_profiles',
    joinColumns: [
      { name: 'profile_id' }
    ],
    inverseJoinColumns: [
      { name: 'feature_id' }
    ]
  })
    features?: Feature[]
}

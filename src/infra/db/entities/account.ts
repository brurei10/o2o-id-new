import { Entity } from './entity'

import { Entity as DbEntity, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import { AccountModel } from '@/domain/models'
import { ProductService } from './product-service'
import { User } from './user'
import { Profile } from './profile'
import { AccountUser } from './account-user'
import { UserAccountProfile } from './user-account-profile'

@DbEntity('tb_accounts')
export class Account extends Entity {
  constructor (input: AccountModel) {
    super()
    Object.assign(this, input)
  }

  @Column({ name: 'user_owner_id' })
    userOwnerId: number

  @Column({ name: 'product_service_id' })
    productServiceId: number

  @Column({ name: 'contract_code' })
    contractCode: string

  @Column({ name: 'cpf_cnpj_owner' })
    cpfCnpjOWner: string

  @Column({ name: 'empresa_contrato' })
    empresaContrato: string

  @Column({ name: 'filial_contrato' })
    filialContrato: string

  @Column({ name: 'product_name' })
    productName: string

  @Column({ name: 'is_active' })
    isActive: boolean

  @OneToMany(() => Profile, profile => profile.account)
    profiles: Profile[]

  @OneToMany(() => AccountUser, accountUser => accountUser.account)
    accountUsers: AccountUser[]

  @OneToMany(() => UserAccountProfile, userAccountProfile => userAccountProfile.account)
    userAccountProfiles: UserAccountProfile[]

  @ManyToOne(() => ProductService, productService => productService.accounts)
  @JoinColumn({ name: 'product_service_id' })
    productService: ProductService

  @ManyToOne(() => User, user => user.accounts)
  @JoinColumn({ name: 'user_owner_id' })
    userOwner: User
}

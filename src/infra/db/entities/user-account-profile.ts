import { Entity } from './entity'

import { Entity as DbEntity, Column, JoinColumn, ManyToOne } from 'typeorm'
import { UserAccountProfileModel } from '@/domain/models'
import { Profile } from './profile'
import { User } from './user'
import { Account } from './account'

@DbEntity('tb_user_account_profiles')
export class UserAccountProfile extends Entity {
  constructor (input: UserAccountProfileModel) {
    super()
    Object.assign(this, input)
  }

  @Column({ name: 'account_id' })
    accountId: number

  @Column({ name: 'profile_id' })
    profileId: number

  @Column({ name: 'user_id' })
    userId: number

  @ManyToOne(() => User, user => user.userAccountProfiles)
  @JoinColumn({ name: 'user_id' })
    user: User

  @ManyToOne(() => Profile, profile => profile.userAccountProfiles)
  @JoinColumn({ name: 'profile_id' })
    profile: Profile

  @ManyToOne(() => Account, account => account.userAccountProfiles)
  @JoinColumn({ name: 'account_id' })
    account: Account
}

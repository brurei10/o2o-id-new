import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class tbUsers1660260906805 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_users',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'active',
          type: 'boolean',
          isNullable: true
        },
        {
          name: 'first_access',
          type: 'boolean',
          isNullable: true
        },
        {
          name: 'email',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'cpf_cnpj',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'id_external',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'login',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'password',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'phone',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'token',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'user_language',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'whats_phone',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'last_access',
          type: 'datetime',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tb_users')
  }
}

import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbUserAccountProfiles1660264653555 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_user_account_profiles',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'profile_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'account_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'user_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_user_account_profiles',
      new TableForeignKey({
        name: 'FK7y9nuinh6gc8hkvox401qwg87',
        columnNames: ['account_id'],
        referencedTableName: 'tb_accounts',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_user_account_profiles',
      new TableForeignKey({
        name: 'FK4egwnxtwyuoyo3u37p1tq31rw',
        columnNames: ['profile_id'],
        referencedTableName: 'tb_profiles',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_user_account_profiles',
      new TableForeignKey({
        name: 'FKebrjwp1kctstnv58efk706emc',
        columnNames: ['user_id'],
        referencedTableName: 'tb_users',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_user_account_profiles', 'FK7y9nuinh6gc8hkvox401qwg87')
    await queryRunner.dropForeignKey('tb_user_account_profiles', 'FK4egwnxtwyuoyo3u37p1tq31rw')
    await queryRunner.dropForeignKey('tb_user_account_profiles', 'FKebrjwp1kctstnv58efk706emc')
    await queryRunner.dropTable('tb_user_account_profiles')
  }
}

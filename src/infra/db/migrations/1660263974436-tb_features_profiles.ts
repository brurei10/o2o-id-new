import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbFeaturesProfiles1660263974436 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_features_profiles',
      columns: [
        {
          name: 'profile_id',
          type: 'bigint(20)',
          isNullable: false,
          isPrimary: true
        },
        {
          name: 'feature_id',
          type: 'bigint(20)',
          isNullable: false,
          isPrimary: true
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_features_profiles',
      new TableForeignKey({
        name: 'FKo99f3n0t77w4sjluurpoat887',
        columnNames: ['feature_id'],
        referencedTableName: 'tb_features',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_features_profiles',
      new TableForeignKey({
        name: 'FK8oglswcfnei5ru1hqh150gbtm',
        columnNames: ['profile_id'],
        referencedTableName: 'tb_profiles',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_features_profiles', 'FKo99f3n0t77w4sjluurpoat887')
    await queryRunner.dropForeignKey('tb_features_profiles', 'FK8oglswcfnei5ru1hqh150gbtm')
    await queryRunner.dropTable('tb_features_profiles')
  }
}

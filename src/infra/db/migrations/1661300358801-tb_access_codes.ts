import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbAccessCodes1661300358801 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_access_codes',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'user_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'code',
          type: 'varchar(255)',
          isNullable: false
        },
        {
          name: 'type',
          type: 'varchar(255)',
          isNullable: false
        },
        {
          name: 'valid_at',
          type: 'datetime',
          isNullable: false
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_access_codes',
      new TableForeignKey({
        name: 'FKaaegjqwhegqwjhegqwjhegqwejh',
        columnNames: ['user_id'],
        referencedTableName: 'tb_users',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_access_codes', 'FKaaegjqwhegqwjhegqwjhegqwejh')
    await queryRunner.dropTable('tb_access_codes')
  }
}

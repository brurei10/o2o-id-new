import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbProfiles1660263763122 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_profiles',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'product_service_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'account_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_profiles',
      new TableForeignKey({
        name: 'FKlc4oipegt3vyph78q31itt3pf',
        columnNames: ['account_id'],
        referencedTableName: 'tb_accounts',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_profiles',
      new TableForeignKey({
        name: 'FKsikbfefi3u74ohnk63940sb27',
        columnNames: ['product_service_id'],
        referencedTableName: 'tb_product_services',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_profiles', 'FKsikbfefi3u74ohnk63940sb27')
    await queryRunner.dropForeignKey('tb_profiles', 'FKlc4oipegt3vyph78q31itt3pf')
    await queryRunner.dropTable('tb_profiles')
  }
}

import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbAccountUsers1660262429620 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_account_users',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'account_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'user_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'is_admin',
          type: 'boolean',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_account_users',
      new TableForeignKey({
        name: 'FK52dce9tk40s6vc8eave7n4erh',
        columnNames: ['user_id'],
        referencedTableName: 'tb_users',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_account_users',
      new TableForeignKey({
        name: 'FKk23j4jgjehfo3chwjvthm88f3',
        columnNames: ['account_id'],
        referencedTableName: 'tb_accounts',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_account_users', 'FK52dce9tk40s6vc8eave7n4erh')
    await queryRunner.dropForeignKey('tb_account_users', 'FKk23j4jgjehfo3chwjvthm88f3')
    await queryRunner.dropTable('tb_account_users')
  }
}

import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class tbLogs1660692929686 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_logs',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'content',
          type: 'longtext',
          isNullable: false
        },
        {
          name: 'type',
          type: 'varchar(255)',
          isNullable: false
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tb_logs')
  }
}

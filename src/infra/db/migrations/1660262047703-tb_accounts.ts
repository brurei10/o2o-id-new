import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbAccounts1660262047703 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_accounts',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment'
        },
        {
          name: 'product_service_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'user_owner_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'contract_code',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'cpf_cnpj_owner',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'empresa_contrato',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'filial_contrato',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'is_active',
          type: 'boolean',
          isNullable: true
        },
        {
          name: 'product_name',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_accounts',
      new TableForeignKey({
        name: 'FKjnt425p2kr66nx2y47td47t6r',
        columnNames: ['product_service_id'],
        referencedTableName: 'tb_product_services',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_accounts',
      new TableForeignKey({
        name: 'FKoxl6ngxnfm2txkjmfgpc2mylc',
        columnNames: ['user_owner_id'],
        referencedTableName: 'tb_users',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_accounts', 'FKjnt425p2kr66nx2y47td47t6r')
    await queryRunner.dropForeignKey('tb_accounts', 'FKoxl6ngxnfm2txkjmfgpc2mylc')
    await queryRunner.dropTable('tb_accounts')
  }
}

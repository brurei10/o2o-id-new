import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbFeatures1660263588830 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_features',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true
        },
        {
          name: 'product_service_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: true
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_features',
      new TableForeignKey({
        name: 'FKlmth2c9cx71iptjowo69ka0rs',
        columnNames: ['product_service_id'],
        referencedTableName: 'tb_product_services',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_features', 'FKlmth2c9cx71iptjowo69ka0rs')
    await queryRunner.dropTable('tb_features')
  }
}

import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbUsersProductServices1660264944831 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_users_product_services',
      columns: [
        {
          name: 'user_id',
          type: 'bigint(20)',
          isNullable: false,
          isPrimary: true
        },
        {
          name: 'product_service_id',
          type: 'bigint(20)',
          isNullable: false,
          isPrimary: true
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_users_product_services',
      new TableForeignKey({
        name: 'FKif4w9gslygyo1tnb9pcvva3mt',
        columnNames: ['product_service_id'],
        referencedTableName: 'tb_product_services',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )

    await queryRunner.createForeignKey(
      'tb_users_product_services',
      new TableForeignKey({
        name: 'FKrlvbxdqlpeu3ubx37ledvf6tf',
        columnNames: ['user_id'],
        referencedTableName: 'tb_users',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_users_product_services', 'FKif4w9gslygyo1tnb9pcvva3mt')
    await queryRunner.dropForeignKey('tb_users_product_services', 'FKrlvbxdqlpeu3ubx37ledvf6tf')
    await queryRunner.dropTable('tb_users_product_services')
  }
}

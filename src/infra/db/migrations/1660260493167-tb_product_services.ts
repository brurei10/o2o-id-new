import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class tbProductServices1660260493167 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_product_services',
      columns: [
        {
          name: 'id',
          type: 'bigint(20)',
          isPrimary: true
        },
        {
          name: 'owner_product_id',
          type: 'bigint(20)',
          isNullable: true
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'description',
          type: 'varchar(255)',
          isNullable: true
        }
      ]
    }))

    await queryRunner.createForeignKey(
      'tb_product_services',
      new TableForeignKey({
        name: 'FKaa1mkqnu0buow0btq6crjk16a',
        columnNames: ['owner_product_id'],
        referencedTableName: 'tb_product_services',
        referencedColumnNames: ['id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tb_product_services', 'FKaa1mkqnu0buow0btq6crjk16a')
    await queryRunner.dropTable('tb_product_services')
  }
}

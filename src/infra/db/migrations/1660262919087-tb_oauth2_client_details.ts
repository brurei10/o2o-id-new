import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class tbOauth2ClientDetails1660262919087 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'tb_oauth2_client_details',
      columns: [
        {
          name: 'id',
          type: 'varchar',
          isPrimary: true,
          generationStrategy: 'uuid'
        },
        {
          name: 'access_token_validity',
          type: 'int(11)',
          isNullable: true
        },
        {
          name: 'authorities',
          type: 'longtext',
          isNullable: true
        },
        {
          name: 'client_id',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'client_secret',
          type: 'varchar(255)',
          isNullable: true
        },
        {
          name: 'grant_types',
          type: 'longtext',
          isNullable: true
        },
        {
          name: 'redirect_uris',
          type: 'longtext',
          isNullable: true
        },
        {
          name: 'refresh_token_validity',
          type: 'int(11)',
          isNullable: true
        },
        {
          name: 'resources',
          type: 'longtext',
          isNullable: true
        },
        {
          name: 'scopes',
          type: 'longtext',
          isNullable: true
        },
        {
          name: 'secret_required',
          type: 'boolean',
          isNullable: true
        },
        {
          name: 'created_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        },
        {
          name: 'updated_at',
          type: 'datetime',
          isNullable: false,
          default: 'CURRENT_TIMESTAMP'
        }
      ]
    }))
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tb_oauth2_client_details')
  }
}

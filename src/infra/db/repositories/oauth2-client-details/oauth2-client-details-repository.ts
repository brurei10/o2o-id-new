import { LoadOauth2ClientDetailsRepository } from '@/domain/protocols'
import { Oauth2ClientDetails, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class Oauth2ClientDetailsRepository extends MainRepository implements LoadOauth2ClientDetailsRepository {
  async load ({ clientId, clientSecret, grantType }: LoadOauth2ClientDetailsRepository.Input): Promise<LoadOauth2ClientDetailsRepository.Output> {
    const oauthRepo = this.getRepository(Oauth2ClientDetails)
    const result = await oauthRepo.manager.createQueryBuilder(Oauth2ClientDetails, 'oauth')
      .where('oauth.clientId = :clientId', { clientId })
      .andWhere('oauth.clientSecret = :clientSecret', { clientSecret })
      .andWhere('oauth.grantTypes like :grantType', { grantType: `%${grantType}%` })
      .getOne()

    return result && mapper(result)
  }
}

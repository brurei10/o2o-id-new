import { SQLConnection, Oauth2ClientDetailsRepository } from '@/infra/db'
import { mockInsertOauth2ClientDetails } from '@/infra/test'
import { MainRepository } from '../main-repository'

const makeSut = (): Oauth2ClientDetailsRepository => new Oauth2ClientDetailsRepository()

describe('Oauth2ClientDetailsRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an oauth on load by id succes', async () => {
    const oauth = await mockInsertOauth2ClientDetails()

    const sut = makeSut()
    const result = await sut.load({ clientId: oauth.clientId, clientSecret: oauth.clientSecret, grantType: oauth.grantTypes })

    expect(result.id).toBeTruthy()
    expect(result.accessTokenValidity).toBe(oauth.accessTokenValidity)
    expect(result.authorities).toBe(oauth.authorities)
    expect(result.clientId).toBe(oauth.clientId)
    expect(result.clientSecret).toBe(oauth.clientSecret)
    expect(result.grantTypes).toBe(oauth.grantTypes)
    expect(result.redirectUris).toBe(oauth.redirectUris)
    expect(result.refreshTokenValidity).toBe(oauth.refreshTokenValidity)
    expect(result.resources).toBe(oauth.resources)
    expect(result.scopes).toBe(oauth.scopes)
    expect(result.secretRequired).toBe(oauth.secretRequired)
  })
})

import { AddAccountRepository } from '@/domain/protocols'
import { Account, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class AccountRepository extends MainRepository implements AddAccountRepository {
  async add (input: AddAccountRepository.Input): Promise<AddAccountRepository.Output> {
    const accountRepo = this.getRepository(Account)
    const result = await accountRepo.manager.save(new Account(input))
    return result && mapper(result)
  }
}

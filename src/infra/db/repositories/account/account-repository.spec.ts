import { mockAccountParams } from '@/domain/test'
import { SQLConnection, AccountRepository } from '@/infra/db'
import { mockInsertProductService, mockInsertUser } from '@/infra/test'
import { MainRepository } from '../main-repository'

const makeSut = (): AccountRepository => new AccountRepository()

describe('AccountRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an account on add succes', async () => {
    const params = mockAccountParams()
    const sut = makeSut()
    const product = await mockInsertProductService()
    const user = await mockInsertUser()
    const account = await sut.add({ ...params, productServiceId: product.id, userOwnerId: user.id })

    expect(account.id).toBeTruthy()
    expect(account.userOwnerId).toBe(user.id)
    expect(account.productServiceId).toBe(product.id)
    expect(account.contractCode).toBe(params.contractCode)
    expect(account.cpfCnpjOWner).toBe(params.cpfCnpjOWner)
    expect(account.empresaContrato).toBe(params.empresaContrato)
    expect(account.filialContrato).toBe(params.filialContrato)
    expect(account.productName).toBe(params.productName)
  })
})

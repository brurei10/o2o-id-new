
import { ObjectType, Repository } from 'typeorm'
import { SQLConnection } from '@/infra/db'

export abstract class MainRepository {
  constructor (private readonly connection: SQLConnection = SQLConnection.getInstance()) {}

  getRepository<Entity> (entity: ObjectType<Entity>): Repository<Entity> {
    return this.connection.getRepository(entity)
  }
}

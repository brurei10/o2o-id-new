import { mockUserParams } from '@/domain/test'
import { SQLConnection, UserRepository } from '@/infra/db'
import { mockInsertProductService, mockInsertUser } from '@/infra/test'
import { faker } from '@faker-js/faker'
import { MainRepository } from '../main-repository'

const makeSut = (): UserRepository => new UserRepository()

describe('UserRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an user on add succes', async () => {
    const params = mockUserParams()
    const sut = makeSut()
    const product = await mockInsertProductService()
    const user = await sut.add({ ...params, productServices: [product] })

    expect(user.id).toBeTruthy()
    expect(user.cpfCnpj).toBe(params.cpfCnpj)
    expect(user.name).toBe(params.name)
    expect(user.email).toBe(params.email)
    expect(user.idExternal).toBe(params.idExternal)
    expect(user.login).toBe(params.login)
    expect(user.password).toBe(params.password)
    expect(user.phone).toBe(params.phone)
    expect(user.userLanguage).toBe(params.userLanguage)
    expect(user.whatsPhone).toBe(params.whatsPhone)
    expect(user.token).toBe(params.token)
    expect(user.firstAccess).toBe(params.firstAccess)
    expect(user.active).toBe(params.active)
  })

  it('should return an user on update password succes', async () => {
    const sut = makeSut()

    const updatedPassword = faker.internet.password()

    const user = await mockInsertUser()

    const result = await sut.updatePassword({ id: user.id, password: updatedPassword })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(updatedPassword)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })

  it('should return an user on load by cpfCnpj succes', async () => {
    const user = await mockInsertUser()

    const sut = makeSut()
    const result = await sut.loadByCpfCnpj({ cpfCnpj: user.cpfCnpj })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(user.password)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })

  it('should return an user on load by email succes', async () => {
    const user = await mockInsertUser()

    const sut = makeSut()
    const result = await sut.loadByEmail({ email: user.email })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(user.password)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })

  it('should return an user on load by login/email succes', async () => {
    const user = await mockInsertUser()

    const sut = makeSut()
    const result = await sut.loadByCredentials({ login: user.email })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(user.password)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })

  it('should return an user on load by login/cpfCnpj succes', async () => {
    const user = await mockInsertUser()

    const sut = makeSut()
    const result = await sut.loadByCredentials({ login: user.cpfCnpj })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(user.password)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })

  it('should return an user on load by login/username succes', async () => {
    const user = await mockInsertUser()

    const sut = makeSut()
    const result = await sut.loadByCredentials({ login: user.login })

    expect(result.id).toBeTruthy()
    expect(result.cpfCnpj).toBe(user.cpfCnpj)
    expect(result.name).toBe(user.name)
    expect(result.email).toBe(user.email)
    expect(result.idExternal).toBe(user.idExternal)
    expect(result.login).toBe(user.login)
    expect(result.password).toBe(user.password)
    expect(result.phone).toBe(user.phone)
    expect(result.userLanguage).toBe(user.userLanguage)
    expect(result.whatsPhone).toBe(user.whatsPhone)
    expect(result.token).toBe(user.token)
    expect(result.firstAccess).toBe(user.firstAccess)
    expect(result.active).toBe(user.active)
  })
})

import { AddUserRepository, LoadUserByCpfCnpjRepository, LoadUserByCredentialsRepository, LoadUserByEmailRepository, UpdatePasswordRepository } from '@/domain/protocols'
import { User, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class UserRepository extends MainRepository implements AddUserRepository, LoadUserByCpfCnpjRepository, LoadUserByEmailRepository, LoadUserByCredentialsRepository {
  async add (input: AddUserRepository.Input): Promise<AddUserRepository.Output> {
    const userRepo = this.getRepository(User)
    const result = await userRepo.manager.save(new User(input))
    return result && mapper(result)
  }

  async updatePassword ({ password, id }: UpdatePasswordRepository.Input): Promise<UpdatePasswordRepository.Output> {
    const userRepo = this.getRepository(User)
    const user = await userRepo.manager.findOne(User, id)
    const result = await userRepo.manager.save(User, {
      ...user,
      password
    })
    return result && mapper(result)
  }

  async loadByCpfCnpj ({ cpfCnpj }: LoadUserByCpfCnpjRepository.Input): Promise<LoadUserByCpfCnpjRepository.Output> {
    const userRepo = this.getRepository(User)
    const result = await userRepo.manager.createQueryBuilder(User, 'user')
      .where('user.cpfCnpj = :cpfCnpj', { cpfCnpj })
      .getOne()

    return result && mapper(result)
  }

  async loadByEmail ({ email }: LoadUserByEmailRepository.Input): Promise<LoadUserByEmailRepository.Output> {
    const userRepo = this.getRepository(User)
    const result = await userRepo.manager.createQueryBuilder(User, 'user')
      .where('user.email = :email', { email })
      .getOne()

    return result && mapper(result)
  }

  async loadByCredentials ({ login }: LoadUserByCredentialsRepository.Input): Promise<LoadUserByCredentialsRepository.Output> {
    const userRepo = this.getRepository(User)
    const result = await userRepo.manager.createQueryBuilder(User, 'user')
      .leftJoinAndSelect('user.productServices', 'product')
      .leftJoinAndSelect('product.accounts', 'accounts')
      .leftJoinAndSelect('accounts.profiles', 'profiles')
      .leftJoinAndSelect('profiles.features', 'features')
      .where(`
          user.email = :login OR
          user.cpfCnpj = :login OR
          user.login = :login
      `, { login })
      .getOne()

    return result && mapper(result)
  }
}

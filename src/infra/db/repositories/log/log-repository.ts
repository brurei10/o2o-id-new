import { SaveLogRepository } from '@/domain/protocols'
import { MainRepository } from '../main-repository'
import { Log, mapper } from '@/infra/db'
import { getRepository } from 'typeorm'

export class LogRepository extends MainRepository implements SaveLogRepository {
  async save (input: SaveLogRepository.Input): Promise<SaveLogRepository.Output> {
    const logRepo = getRepository(Log)
    const log = await logRepo.manager.save(new Log(input))
    return log && mapper(log)
  }
}

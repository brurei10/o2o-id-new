import { SQLConnection, MainRepository, LogRepository } from '@/infra/db'

import { faker } from '@faker-js/faker'

const makeSut = (): LogRepository => new LogRepository()

describe('LogRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an log on add succes', async () => {
    const sut = makeSut()

    const params = {
      content: faker.lorem.words(),
      type: faker.random.word()
    }
    const log = await sut.save(params)

    expect(log.id).toBeTruthy()
    expect(log.content).toBe(params.content)
    expect(log.type).toBe(params.type)
    expect(log.createdAt).toBeTruthy()
  })
})

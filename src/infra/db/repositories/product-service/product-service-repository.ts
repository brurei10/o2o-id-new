import { AddProductServiceRepository, LoadProductServiceByIdRepository } from '@/domain/protocols'
import { ProductService, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class ProductServiceRepository extends MainRepository implements AddProductServiceRepository, LoadProductServiceByIdRepository {
  async add (input: AddProductServiceRepository.Input): Promise<AddProductServiceRepository.Output> {
    const productRepo = this.getRepository(ProductService)
    const result = await productRepo.manager.save(new ProductService(input))
    return result && mapper(result)
  }

  async loadById ({ id }: LoadProductServiceByIdRepository.Input): Promise<LoadProductServiceByIdRepository.Output> {
    const productRepo = this.getRepository(ProductService)
    const result = await productRepo.manager.createQueryBuilder(ProductService, 'product')
      .where('product.id = :id', { id })
      .getOne()

    return result && mapper(result)
  }
}

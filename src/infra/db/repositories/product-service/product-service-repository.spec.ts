import { mockProductServiceParams } from '@/domain/test'
import { SQLConnection, ProductServiceRepository } from '@/infra/db'
import { mockInsertProductService } from '@/infra/test'
import { MainRepository } from '../main-repository'

const makeSut = (): ProductServiceRepository => new ProductServiceRepository()

describe('ProductServiceRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an user on add succes', async () => {
    const params = mockProductServiceParams()
    const sut = makeSut()
    const product = await sut.add(params)

    expect(product.id).toBeTruthy()
    expect(product.description).toBe(params.description)
    expect(product.name).toBe(params.name)
    expect(product.ownerProductId).toBe(params.ownerProductId)
  })

  it('should return an user on load by id succes', async () => {
    const product = await mockInsertProductService()

    const sut = makeSut()
    const result = await sut.loadById({ id: product.id })

    expect(result.id).toBeTruthy()
    expect(result.description).toBe(product.description)
    expect(result.name).toBe(product.name)
    expect(result.ownerProductId).toBe(product.ownerProductId)
  })
})

import { mockAccessCodeParams } from '@/domain/test'
import { SQLConnection, AccessCodeRepository } from '@/infra/db'
import { MainRepository } from '../main-repository'

import { mockInsertAccessCode, mockInsertUser } from '@/infra/test'

const makeSut = (): AccessCodeRepository => new AccessCodeRepository()

describe('AccessCodeRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an user on add succes', async () => {
    const user = await mockInsertUser()
    const params = mockAccessCodeParams()
    const sut = makeSut()
    const accessCode = await sut.save({ ...params, userId: user.id })

    expect(accessCode.id).toBeTruthy()
    expect(accessCode.createdAt).toBeTruthy()
    expect(accessCode.updatedAt).toBeTruthy()
    expect(accessCode.validAt).toBeTruthy()
    expect(accessCode.userId).toBe(user.id)
    expect(accessCode.code).toBe(params.code)
  })

  it('should return an user on load by user succes', async () => {
    const sut = makeSut()
    const accessCode = await mockInsertAccessCode()

    const result = await sut.loadByUser({ email: accessCode.user.email })

    expect(result.id).toBeTruthy()
    expect(result.createdAt).toBeTruthy()
    expect(result.updatedAt).toBeTruthy()
    expect(result.validAt).toBeTruthy()
    expect(result.userId).toBe(accessCode.userId)
    expect(result.code).toBe(accessCode.code)
  })

  it('should return an user on load by id succes', async () => {
    const sut = makeSut()
    const accessCode = await mockInsertAccessCode()

    const result = await sut.loadById({ id: accessCode.id })

    expect(result.id).toBeTruthy()
    expect(result.createdAt).toBeTruthy()
    expect(result.updatedAt).toBeTruthy()
    expect(result.validAt).toBeTruthy()
    expect(result.userId).toBe(accessCode.userId)
    expect(result.code).toBe(accessCode.code)
  })

  it('should return a affected row on delete succes', async () => {
    const sut = makeSut()
    const accessCode = await mockInsertAccessCode()

    const result = await sut.delete({ id: accessCode.id })

    expect(result).toBeGreaterThan(0)
  })
})

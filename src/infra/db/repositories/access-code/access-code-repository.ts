import { LoadAccessCodeByIdRepository, LoadAccessCodeByUserEmailRepository, SaveAccessCodeRepository, DeleteAccessCodeRepository } from '@/domain/protocols'
import { AccessCode, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class AccessCodeRepository extends MainRepository implements SaveAccessCodeRepository, LoadAccessCodeByIdRepository, LoadAccessCodeByUserEmailRepository, DeleteAccessCodeRepository {
  async save (input: SaveAccessCodeRepository.Input): Promise<SaveAccessCodeRepository.Output> {
    const accessCodeRepo = this.getRepository(AccessCode)
    const user = await accessCodeRepo.manager.save(new AccessCode(input))
    return user && mapper(user)
  }

  async loadByUser ({ email }: LoadAccessCodeByUserEmailRepository.Input): Promise<LoadAccessCodeByUserEmailRepository.Output> {
    const accessCodeRepo = this.getRepository(AccessCode)
    const query = accessCodeRepo.manager.createQueryBuilder(AccessCode, 'accessCode')
      .leftJoinAndSelect('accessCode.user', 'user')
      .where('user.email = :email', { email })

    const result = await query.getOne()

    return result && mapper(result)
  }

  async loadById ({ id }: LoadAccessCodeByIdRepository.Input): Promise<LoadAccessCodeByIdRepository.Output> {
    const accessCodeRepo = this.getRepository(AccessCode)
    const query = accessCodeRepo.manager.createQueryBuilder(AccessCode, 'access_code')
      .where('access_code.id = :id', { id })

    const result = await query.getOne()

    return result && mapper(result)
  }

  async delete ({ id }: DeleteAccessCodeRepository.Input): Promise<DeleteAccessCodeRepository.Output> {
    const accessCodeRepo = this.getRepository(AccessCode)
    const result = await accessCodeRepo.manager.delete(AccessCode, { id })
    return result.affected
  }
}

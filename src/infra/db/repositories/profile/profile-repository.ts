import { AddProfileRepository } from '@/domain/protocols'
import { Profile, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class ProfileRepository extends MainRepository implements AddProfileRepository {
  async add (input: AddProfileRepository.Input): Promise<AddProfileRepository.Output> {
    const profileRepo = this.getRepository(Profile)
    const profile = new Profile(input)
    const result = await profileRepo.manager.save(profile)
    return result && mapper(result)
  }
}

import { mockProfileParams } from '@/domain/test'
import { SQLConnection, ProfileRepository } from '@/infra/db'
import { mockInsertAccount, mockInsertFeature } from '@/infra/test'
import { MainRepository } from '../main-repository'

const makeSut = (): ProfileRepository => new ProfileRepository()

describe('ProfileRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an profile on add succes', async () => {
    const params = mockProfileParams()
    const sut = makeSut()
    const account = await mockInsertAccount()
    const features = [await mockInsertFeature()]
    const profile = await sut.add({
      ...params,
      accountId: account.id,
      productServiceId: features[0].productServiceId,
      features
    })

    expect(profile.id).toBeTruthy()
    expect(profile.accountId).toBe(account.id)
    expect(profile.productServiceId).toBe(features[0].productServiceId)
    expect(profile.name).toBe(params.name)
  })
})

import { LoadFeatureByIdRepository } from '@/domain/protocols'
import { Feature, mapper } from '@/infra/db'
import { MainRepository } from '../main-repository'

export class FeatureRepository extends MainRepository implements LoadFeatureByIdRepository {
  async loadById ({ id }: LoadFeatureByIdRepository.Input): Promise<LoadFeatureByIdRepository.Output> {
    const featureRepo = this.getRepository(Feature)
    const result = await featureRepo.manager.createQueryBuilder(Feature, 'feature')
      .where('feature.id = :id', { id })
      .getOne()

    return result && mapper(result)
  }
}

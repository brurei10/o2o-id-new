import { SQLConnection, FeatureRepository } from '@/infra/db'
import { mockInsertFeature } from '@/infra/test'
import { MainRepository } from '../main-repository'

const makeSut = (): FeatureRepository => new FeatureRepository()

describe('FeatureRepository', () => {
  beforeAll(async () => {
    await SQLConnection.getInstance().connect()
  })

  afterAll(async () => {
    await SQLConnection.getInstance().disconnect()
  })

  it('should extend MainRepository', async () => {
    const sut = makeSut()
    expect(sut).toBeInstanceOf(MainRepository)
  })

  it('should return an user on load by id succes', async () => {
    const feature = await mockInsertFeature()

    const sut = makeSut()
    const result = await sut.loadById({ id: feature.id })

    expect(result.id).toBeTruthy()
    expect(result.name).toBe(feature.name)
    expect(result.productServiceId).toBe(feature.productServiceId)
  })
})

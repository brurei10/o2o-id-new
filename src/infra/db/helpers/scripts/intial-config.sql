
CREATE TABLE `product_service` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `owner_product_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaa1mkqnu0buow0btq6crjk16a` (`owner_product_id`),
  CONSTRAINT `FKaa1mkqnu0buow0btq6crjk16a` FOREIGN KEY (`owner_product_id`) REFERENCES `product_service` (`id`)
) ENGINE=InnoDB ;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `cpf_cnpj` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `first_access` bit(1) DEFAULT NULL,
  `id_external` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `user_language` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `whats_phone` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 ;

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contract_code` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cpf_cnpj_owner` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `empresa_contrato` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `filial_contrato` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_service_id` bigint(20) DEFAULT NULL,
  `user_owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjnt425p2kr66nx2y47td47t6r` (`product_service_id`),
  KEY `FKoxl6ngxnfm2txkjmfgpc2mylc` (`user_owner_id`),
  CONSTRAINT `FKjnt425p2kr66nx2y47td47t6r` FOREIGN KEY (`product_service_id`) REFERENCES `product_service` (`id`),
  CONSTRAINT `FKoxl6ngxnfm2txkjmfgpc2mylc` FOREIGN KEY (`user_owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 ;


CREATE TABLE `account_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_admin` bit(1) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk23j4jgjehfo3chwjvthm88f3` (`account_id`),
  KEY `FK52dce9tk40s6vc8eave7n4erh` (`user_id`),
  CONSTRAINT `FK52dce9tk40s6vc8eave7n4erh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKk23j4jgjehfo3chwjvthm88f3` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 ;


CREATE TABLE `oauth2_client_details` (
  `id` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `authorities` longtext COLLATE utf8mb4_0900_ai_ci,
  `client_id` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `client_secret` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `grant_types` longtext COLLATE utf8mb4_0900_ai_ci,
  `redirect_uris` longtext COLLATE utf8mb4_0900_ai_ci,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `resources` longtext COLLATE utf8mb4_0900_ai_ci,
  `scopes` longtext COLLATE utf8mb4_0900_ai_ci,
  `secret_required` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;


CREATE TABLE `feature` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_service_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlmth2c9cx71iptjowo69ka0rs` (`product_service_id`),
  CONSTRAINT `FKlmth2c9cx71iptjowo69ka0rs` FOREIGN KEY (`product_service_id`) REFERENCES `product_service` (`id`)
) ENGINE=InnoDB ;


CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `product_service_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlc4oipegt3vyph78q31itt3pf` (`account_id`),
  KEY `FKsikbfefi3u74ohnk63940sb27` (`product_service_id`),
  CONSTRAINT `FKlc4oipegt3vyph78q31itt3pf` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FKsikbfefi3u74ohnk63940sb27` FOREIGN KEY (`product_service_id`) REFERENCES `product_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 ;


CREATE TABLE `feature_profile` (
  `profile_id` int(11) NOT NULL,
  `feature_id` bigint(20) NOT NULL,
  PRIMARY KEY (`profile_id`,`feature_id`),
  KEY `FKo99f3n0t77w4sjluurpoat887` (`feature_id`),
  CONSTRAINT `FK8oglswcfnei5ru1hqh150gbtm` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`),
  CONSTRAINT `FKo99f3n0t77w4sjluurpoat887` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`)
) ENGINE=InnoDB ;

CREATE TABLE `user_account_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7y9nuinh6gc8hkvox401qwg87` (`account_id`),
  KEY `FK4egwnxtwyuoyo3u37p1tq31rw` (`profile_id`),
  KEY `FKebrjwp1kctstnv58efk706emc` (`user_id`),
  CONSTRAINT `FK4egwnxtwyuoyo3u37p1tq31rw` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`),
  CONSTRAINT `FK7y9nuinh6gc8hkvox401qwg87` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FKebrjwp1kctstnv58efk706emc` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 ;

CREATE TABLE `user_product_service` (
  `user_id` bigint(20) NOT NULL,
  `product_service_id` bigint(20) NOT NULL,
  PRIMARY KEY (`product_service_id`,`user_id`),
  KEY `FKrlvbxdqlpeu3ubx37ledvf6tf` (`user_id`),
  CONSTRAINT `FKif4w9gslygyo1tnb9pcvva3mt` FOREIGN KEY (`product_service_id`) REFERENCES `product_service` (`id`),
  CONSTRAINT `FKrlvbxdqlpeu3ubx37ledvf6tf` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB ;
import { ConnectionNotFoundError, TransactionNotFoundError } from '../errors'

import { createConnection, getConnection, getConnectionManager, ObjectType, QueryRunner, Repository, Connection, getRepository } from 'typeorm'
import { DbTransaction } from '@/application/contracts'

export class SQLConnection implements DbTransaction {
  private static instance?: SQLConnection
  private query?: QueryRunner
  private connection?: Connection

  private constructor () { }

  static getInstance (): SQLConnection {
    if (SQLConnection.instance === undefined) SQLConnection.instance = new SQLConnection()
    return SQLConnection.instance
  }

  async connect (): Promise<void> {
    this.connection = getConnectionManager().has('default')
      ? getConnection()
      : process.env.NODE_ENV === 'test' ? await this.makeSqliteConnection() : await createConnection()
  }

  async disconnect (): Promise<void> {
    if (this.connection === undefined) throw new ConnectionNotFoundError()
    await getConnection().close()
    this.query = undefined
    this.connection = undefined
  }

  async openTransaction (): Promise<void> {
    if (this.connection === undefined) throw new ConnectionNotFoundError()
    this.query = this.connection.createQueryRunner()
    await this.query.startTransaction()
  }

  async closeTransaction (): Promise<void> {
    if (this.query === undefined) throw new TransactionNotFoundError()
    await this.query.release()
  }

  async commit (): Promise<void> {
    if (this.query === undefined) throw new TransactionNotFoundError()
    await this.query.commitTransaction()
  }

  async rollback (): Promise<void> {
    if (this.query === undefined) throw new TransactionNotFoundError()
    await this.query.rollbackTransaction()
  }

  async clear (): Promise<void> {
    const entities = this.connection.entityMetadatas
    const entityDeletionPromises = entities.map((entity) => async () => {
      const repository = this.connection.getRepository(entity.name)
      await repository.query(`DELETE FROM ${entity.tableName}`)
    })
    await Promise.all(entityDeletionPromises)
  }

  getRepository<Entity> (entity: ObjectType<Entity>): Repository<Entity> {
    if (this.connection === undefined) throw new ConnectionNotFoundError()
    if (this.query !== undefined) return this.query.manager.getRepository(entity)
    return getRepository(entity)
  }

  async makeSqliteConnection (): Promise<Connection> {
    return await createConnection({
      type: 'mysql',
      database: 'o2o-id',
      port: 3306,
      username: 'root',
      password: 'root',
      dropSchema: true,
      entities: ['src/infra/db/entities/index.ts'],
      synchronize: true,
      migrationsRun: true,
      logging: false
    })
  }
}

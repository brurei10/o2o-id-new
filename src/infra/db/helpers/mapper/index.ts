
export const mapper = (data: any): any => {
  return Object.assign({}, data)
}

export const mapCollection = (collection: any[]): any[] => {
  return collection.map(c => mapper(c))
}

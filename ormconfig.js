module.exports = {
  type: 'mysql',
  bigNumberStrings: false,
  extra: {
    decimalNumbers: true
  },
  host: process.env.DB_HOSTNAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
  logging: false,
  synchroize: true,
  migrationsRun: true,
  entities: [process.env.ENTITY_CONFIG],
  migrations: [process.env.MIGRATIONS_CONFIG],
  cli: {
    entitiesDir: process.env.ENTITIES_DIR,
    migrationsDir: process.env.MIGRATIONS_DIR
  }
}
